# AnytimeLibrary

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.0.

## Application Overview

### User of the application

1. User
2. Admin

### Admin Functionality

1. Able to login by giving following credentials : <br/>
   a. userId : admin <br/>
   b. password : password<br/>
2. Admin dashborad with following menu items : <br/>
   a. All books <br/>
   b. Issued Books <br/>
   c. Add Books <br/>
   d. Find Books and Update/Delete<br>
   c. Configuration<br/>
3. All Books : <br/>a. Admin can view all the book with info like Title, author, book image, isbn, rating, location, reviews, availble and total count, Description.<br/>
   b. Admin can search, sort, filter the books.<br/>
   c. Pagination is implemented.<br/>
4. Issued Books : <br/>
   a. Admin can see all the issued books with the books info along with issued adn return date user id and name<br/>
   b. Admin can fetch the issue list of specifc user by providing the UserId<br/>
   c. Admin can search , sort , filter by issued date and status, paginate the book list.<br/>
5. Add Book : <br/>
   a. Admin can find the book by ISBN and add it to the Library<br/>
   b. If book details found from ISBN sever the form will be auto-populated with in info.<br/>
   c. Admin has to provide the info like count and location of the books.<br/>
   d. If book already exists, he will not able to add the book again.
   Insted he can update or delete it.<br>
6. Update/Delete Book : <br/>
   a. Admin can find the book by ISBN and Update or delete from the <br/>
   d. If book is not found. he will get the option to find and add it.<br>
7. Admin Can edit the following configurations : <br>
   a. Issue days<br> b. Renew days <br> c. Max no of issue per user.

### User Functionality

1. User can login by google
2. User dashborad with following menu items : <br/>
   a. All books <br/>
   b. My Books <br/>
   c. Profile Info <br>
3. All Books : <br/>a. User can view all the book with info like Title, author, book image, isbn, rating, location, reviews, availble and total count, Description.<br/>
   b. Use can search, sort, filter the books.<br/>
   c. Pagination is implemented.<br/>
   d. If the book is available, user can issue to him self.<br/>
   e. Return date is set by the admin configuration.<br/>
   f. User can not issue if he execdes the maximun limit of issue set by the user<br>
4. My Books : <br/>
   a. User can see all the issued books of his with the books info along with issued and return date and return date<br/>
   b. user can search , sort , filter by issued date and status, paginate the book list.<br/>
   c. User can return or renew the book for configured days.<br/>
   d. User can provide/edit the review for the returned book.
5. User can view his profile information with book issued status. Also he can set the fav categories

## Performance Test Snapshot

![Performance Snapshot](./src/assets/images/performance.png)

## Test Coverage

![Test Coverage Snapshot1](./src/assets/images/coverage1.png)
![Test Coverage Snapshot2](./src/assets/images/coverage2.png)
![Test Coverage Snapshot3](./src/assets/images/coverage3.png)

## Implementation

### 1. Angular 8

    1. Pipes (ReviwFilter Pipe)
    2. Directives (Highlighter directive)
    3. Reactive Forms
    4. Routing
    6. Lazy Loading
    7. Route Guards
    8. Notifcation Service
    9. Authentication Service
    10. Folder Structure core, shared, feature(admin, user)
    11. Use of scss
    12. Error Handler
    13. Usage of Schematics for generating pipes, directives ,component, modules and service

### 2. Angular Material 8

    1. Custom Theme
    2. Card, Buttons, Inputs, Form Field
    3. Pagination
    4. Sort
    5. Dailog Box

### 3. Google Authentication

### 4. Responsive Web Design (RWD) using Boostrap-grid 4

      1. Mobile
      2. Tablet
      3. Desktops

### 5. Progressive Web App (PWA) using service worker

    User will be able to use the application even in the offline (Data which are loaded once). and gets the notifcation if any update.

### 6. Accessibility

### 7. Lint Plugins to clear lints errors

### 8. Browser compatibility

    1. Chrome
    2. Firefox
    3. Edge

### 9. Karma and Jasmine for Unit Testing

## User Acceptance Testing

### 1. Jamuna M105305

- Overalll functionality is working good
- Using Side Notification insted of top
- Icon for issue to self
- Add , Update or delete can be combine with All books page with a popup
- Location master while add and updating the books

## Requirements

- **Node and Npm** : Follow official site to install node and npm. [link](https://nodejs.org/en/)

- **Angular CLI** : [Explore Angular CLI](https://cli.angular.io/)  
   `npm install -g @angular/cli`

- **Angular Material** :
  Dependency is added in this poc and configured. [Explore Angular Material](https://material.angular.io/)

- **JSON Server CLI** : [Explore Json Server](https://github.com/typicode/json-server)  
   `npm install -g json-server`

- **HTTP Server** :[Explore Http Server](https://www.npmjs.com/package/http-server)  
   `npm install -g http-server`

## Installing Node Modules

Run `npm install`

## Running Server (Json-Server required)

Run `npm run server` for running json server. Navigate to `http://localhost:3000/`.

## Running Application server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/anytime-library` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run coverage` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running PWA

Build the project in the production mode. Server the application using http-server in the port 4200 with json-server running

    ng build --prod
    npm run json-server
    npm run webserver

## Application Screenshots

### Admin

1. Login page
   ![Login page](./src/assets/images/admin/login.png)

2. Dashboard
   ![Dashboard](./src/assets/images/admin/dashboard.png)

3. Configuration Setting
   ![Configuration Setting](./src/assets/images/admin/configuration.png)

4. All Books
   ![All Books](./src/assets/images/admin/allBooks.png)

5. Issued Books
   ![Issued Books](./src/assets/images/admin/issedBooks.png)

6. Add Books
   ![Add Books](./src/assets/images/admin/addBooks.png)

7. Add Books
   ![Add Books](./src/assets/images/admin/addBooks2.png)

8. Find Books
   ![Find Books](./src/assets/images/admin/findBook.png)

9. Update or Delete
   ![Find Books](./src/assets/images/admin/updateDeleteBook.png)

10. View Review
    ![ View Review ](./src/assets/images/admin/review.png)

### User

1. Login page
   ![Login page](./src/assets/images/user/login.png)

2. Goole Login page
   ![Goole Login page](./src/assets/images/user/googleLogin.png)

3. Dashboard
   ![Dashboard](./src/assets/images/user/dashboard.png)

4. Profile View
   ![Profile View](./src/assets/images/user/profile.png)

5. All Books
   ![All Books](./src/assets/images/user/allBooks.png)

6. My Books
   ![My Books](./src/assets/images/user/myBooks.png)

7. View Review
   ![ View Review ](./src/assets/images/user/review.png)

8. Add or Edit Review
   ![Add or Edit Review](./src/assets/images/user/addOrDeleteReview.png)

## Commit History

![ Commit History](./src/assets/images/commitHistory1.png)
![ Commit History](./src/assets/images/commitHistory2.png)
