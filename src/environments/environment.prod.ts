export const environment = {
  production: true,
  API_URL: 'http://localhost:3000',
  ISBN_URL: 'https://www.googleapis.com/books/v1/volumes'
};
