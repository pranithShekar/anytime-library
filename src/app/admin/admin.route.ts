import { AdminRouteGuard } from './../core/route-guards/admin.route.guard';
import { BookListComponent } from './../shared/book-list/book-list.component';
import { AdminMenu } from './admin.menu';
import { AdminComponent } from './admin.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { UserRoleEnum } from '../shared/atl.enum';
import { BookComponent } from './book/book.component';

/**
 * Admin route Configuration
 */
const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminRouteGuard],
    canActivateChild: [AdminRouteGuard],

    children: [
      {
        path: 'dashboard',
        component: AdminDashboardComponent
      },
      {
        path: 'books/:isbn',
        component: BookListComponent,
        data: { role: UserRoleEnum.ADMIN, type: 'books' }
      },
      {
        path: 'add/:isbn',
        component: BookComponent,
        data: { type: 'add' }
      },
      {
        path: 'update/:isbn',
        component: BookComponent,
        data: { type: 'update' }
      },
      ...AdminMenu,
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
