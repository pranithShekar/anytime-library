import { Router, ActivatedRoute } from '@angular/router';
import { AdminMenu } from './admin.menu';
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../shared/menu/menu.service';
/**
 * Admin Component
 */
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  menu = AdminMenu;

  constructor(private menuService: MenuService, private activatedRoute: ActivatedRoute) {
    this.menuService.changeMenu(this.menu, this.activatedRoute, 'dashboard');
  }

  ngOnInit() {}
}
