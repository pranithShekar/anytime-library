import { BookListComponent } from './../shared/book-list/book-list.component';
import { BookComponent } from './book/book.component';
import { Menu } from './../shared/menu/menu.model';
import { IssuedBookListComponent } from '../shared/issued-book-list/issued-book-list.component';
import { UserRoleEnum } from '../shared/atl.enum';

/**
 * Amdin menu Configuration
 */
export const AdminMenu: Menu[] = [
  {
    path: 'books',
    component: BookListComponent,
    title: 'All Books',
    data: { role: UserRoleEnum.ADMIN, type: 'books' }
  },
  {
    path: 'issued',
    component: IssuedBookListComponent,
    title: 'Issued Books',
    data: { role: UserRoleEnum.ADMIN, type: 'issued' }
  },
  {
    path: 'add',
    component: BookComponent,
    title: 'Add Book',
    data: { type: 'add' }
  },
  {
    path: 'update',
    component: BookComponent,
    title: 'Update / Delete Book',
    data: { type: 'update' }
  }
];
