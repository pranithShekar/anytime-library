import { By } from '@angular/platform-browser';
import { MenuService } from './../shared/menu/menu.service';
import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AdminComponent } from './admin.component';

/**
 * Admin Component Testing
 */
describe('Admin -> AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;
  let mockMenuService;

  beforeEach(async(() => {
    mockMenuService = jasmine.createSpyObj(['changeMenu']);
    TestBed.configureTestingModule({
      declarations: [AdminComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: null
        },
        {
          provide: MenuService,
          useValue: mockMenuService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call changeMenu of menu service', () => {
    expect(mockMenuService.changeMenu).toHaveBeenCalled();
  });

  it('should render one router-outlet', () => {
    const expected = 1;

    const actual = fixture.debugElement.queryAll(By.css('router-outlet')).length;

    expect(actual).toBe(expected);
  });
});
