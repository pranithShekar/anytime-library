import { environment } from 'src/environments/environment.prod';
import { TestBed } from '@angular/core/testing';

import { AdminService } from './admin.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ErrorHandlerService } from '../shared/errorHandler.service';
import { Book } from '../shared/model/book.model';

/**
 * Admin Service Testing
 */
describe('Admin -> AdminService', () => {
  let mockErrorHandlerService;
  let service: AdminService;
  let httpTestingController: HttpTestingController;
  const book: Book = {
    id: '1111',
    isbn: '1111',
    title: 'title',
    author: 'author',
    description: 'description',
    rating: 1,
    count: 3,
    availableCount: 3,
    location: 'location',
    imageUrl: 'imageUrl',
    category: 'category'
  };
  beforeEach(() => {
    mockErrorHandlerService = jasmine.createSpyObj(['handleError']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AdminService,
        {
          provide: ErrorHandlerService,
          useValue: mockErrorHandlerService
        }
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(AdminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(' should call getConfig with correct url and data', () => {
    service.getConfig().subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/config`);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call updateConfig with correct url and data', () => {
    const data = {
      maxIssue: 1
    };
    service.updateConfig(data).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/config`);
    expect(request.request.method).toEqual('PATCH');
    expect(request.request.body).toEqual(data);

    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getBook with correct url and data', () => {
    service.getBook(book.id).subscribe(() => {});
    const request = httpTestingController.expectOne(environment.API_URL + '/books/' + book.id);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getBook with correct url and data without error handling', () => {
    service.getBook(book.id, false).subscribe(() => {});
    const request = httpTestingController.expectOne(environment.API_URL + '/books/' + book.id);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getBookFromIsbnServer with correct url and data', () => {
    service.getBookFromIsbnServer(book.id).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.ISBN_URL + '?q=isbn:' + book.id);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call addBook with correct url and data', () => {
    service.addBook(book).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/books`);
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual(book);

    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call updateBook with correct url and data', () => {
    service.updateBook(book).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/books/` + book.id);
    expect(request.request.method).toEqual('PATCH');
    expect(request.request.body).toEqual(book);

    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call deleteBook with correct url and data', () => {
    service.deleteBook(book.id).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/books/` + book.id);
    expect(request.request.method).toEqual('DELETE');

    request.flush('success');
    httpTestingController.verify();
  });
});
