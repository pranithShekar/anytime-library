import { Book } from './../shared/model/book.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { log } from 'util';
import { environment } from 'src/environments/environment.prod';
import { ErrorHandlerService } from '../shared/errorHandler.service';

/**
 * Admin Service
 */
@Injectable()
export class AdminService {
  private url = environment.API_URL;
  private isbnUrl = environment.ISBN_URL;

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) {}

  /** 
   * Get Config
   * @resturn Observable<any>
   */  
  getConfig(): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.url + `/config`, options).pipe(
      tap(config => log(`fetched cofig`)),
      catchError(this.errorHandlerService.handleError('getConfig', null))
    );
  }
  
  /** 
   * Update Config
   * @param config
   * @resturn Observable<any>
   */
  updateConfig(config: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.patch(this.url + `/config`, config, options).pipe(
      tap(configData => log(`updated cofig`)),
      catchError(this.errorHandlerService.handleError('updateConfig', null))
    );
  }

  /** 
   * Get Book
   * @param bookId
   * @param handleError
   * @resturn Observable<any>
   */
    getBook(bookId: string, handleError: boolean = true): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    if (handleError) {
      return this.http.get(this.url + '/books/' + bookId, options).pipe(
        tap(books => log(`fetched book`)),
        catchError(this.errorHandlerService.handleError('getBook', null))
      );
    } else {
      return this.http.get(this.url + '/books/' + bookId, options).pipe(
        tap(books => log(`fetched book`)),
        catchError(() => of(null))
      );
    }
  }

  /** 
   * Get Book From Isbn Server
   * @param bookId
   * @resturn Observable<any>
   * */
    getBookFromIsbnServer(bookId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.isbnUrl + '?q=isbn:' + bookId, options).pipe(
      tap(books => log(`fetched book from Isbn Server`)),
      catchError(this.errorHandlerService.handleError('getBookFromIsbnServer', null))
    );
  }

  /** 
   * Add Book
   * @param book
   * @resturn Observable<any>
   */
    addBook(book: Book): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.post(this.url + '/books', book, options).pipe(
      tap(books => log(`added books`)),
      catchError(this.errorHandlerService.handleError('addBooks', null))
    );
  }

  /** 
   * Update Book
   * @param book
   * @resturn Observable<any>
   */ 
   updateBook(book: Book): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.patch(this.url + '/books/' + book.id, book, options).pipe(
      tap(books => log(`Update book`)),
      catchError(this.errorHandlerService.handleError('updateBook', null))
    );
  }

  /** 
   * Delete Book
   * @param bookId
   * @resturn Observable<any>
   */  
  deleteBook(bookId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.delete(this.url + '/books/' + bookId, options).pipe(
      tap(books => log(`Deleted book`)),
      catchError(this.errorHandlerService.handleError('deleteBook', null))
    );
  }
}
