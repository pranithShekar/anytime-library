import { AdminService } from './admin.service';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminRoutingModule } from './admin.route';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { BookComponent } from './book/book.component';
import { AdminMaterialModule } from './admin.material';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AdminConfigurationComponent } from './admin-configuration/admin-configuration.component';

/**
 * Amdin Module
 */
@NgModule({
  declarations: [AdminComponent, AdminDashboardComponent, BookComponent, AdminConfigurationComponent],
  imports: [CommonModule, AdminMaterialModule, AdminRoutingModule, HttpClientModule, ReactiveFormsModule, SharedModule],
  providers: [AdminService],
  entryComponents: [AdminConfigurationComponent]
})
export class AdminModule {}
