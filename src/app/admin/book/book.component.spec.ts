import { AdminService } from './../admin.service';
import { MatSelectModule } from '@angular/material/select';
import { AuthenticationService } from './../../core/authentication.service';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatFormFieldModule, MatInputModule } from '@angular/material';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserRoleEnum } from 'src/app/shared/atl.enum';
import { BookComponent } from './book.component';
import { Book, IssuedBook } from 'src/app/shared/model/book.model';
import { ReviewFilterPipe } from 'src/app/shared/review-filter.pipe';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedService } from 'src/app/shared/shared.service';
import { NotificationService } from 'src/app/shared/notification.service';

/**
 * Admin Book Find/Add/Update/Delete  Testing
 */

describe('Admin -> BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;
  let mockSharedService;
  let mockNotificationService;
  let authenticationService;
  let mockAdminService;

  const user = {
    email: 'abc@gmail.com',
    name: 'ABC',
    imageUrl: '/abc.jpeg',
    role: UserRoleEnum.USER,
    userId: '123'
  };

  const book: Book = {
    id: '1111',
    isbn: '1111',
    title: 'title',
    author: 'author',
    description: 'description',
    rating: 1,
    count: 3,
    availableCount: 3,
    location: 'location',
    imageUrl: 'imageUrl',
    category: 'category'
  };

  const issuedBook: IssuedBook = {
    id: '1111',
    isbn: '1111',
    title: 'title',
    author: 'author',
    location: 'location',
    imageUrl: 'imageUrl',
    userId: '123',
    userName: 'Anc',
    issuedDate: '12-Aug-2019',
    returnDate: '22-Aug-2019',
    status: 'issued'
  };

  beforeEach(async(() => {
    mockSharedService = jasmine.createSpyObj(['getCategories']);
    mockNotificationService = jasmine.createSpyObj(['show']);
    mockAdminService = jasmine.createSpyObj(['getCategories']);

    mockSharedService.getCategories.and.returnValue(of(true));

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        RouterTestingModule.withRoutes([]),
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule
      ],
      declarations: [BookComponent, ReviewFilterPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: SharedService,
          useValue: mockSharedService
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService
        },
        {
          provide: AdminService,
          useValue: mockAdminService
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              data: {
                type: 'add',
                role: UserRoleEnum.USER
              },
              params: {
                isbn: '11'
              }
            }
          }
        },
        {
          provide: MatDialog,
          useValue: null
        },

        { provide: MAT_DIALOG_DATA, useValue: {} },
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {}
          }
        }
      ]
    }).compileComponents();

    authenticationService = TestBed.get(AuthenticationService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;
    component.createForm();
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    expect(component).toBeTruthy();
  }));
});
