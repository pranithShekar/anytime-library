import { NotificationService } from './../../shared/notification.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { AdminService } from './../admin.service';
import { MenuService } from 'src/app/shared/menu/menu.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Book } from './../../shared/model/book.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { SharedService } from 'src/app/shared/shared.service';

/**
 * Admin Book Find/Add/Update/Delete  Componet
 */
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit, OnDestroy {
  type: string;
  isLoading = false;
  showBookDetailsNotFound = false;
  showBookNotFound = false;
  showBookExists = false;
  title: string;
  showBook = false;
  bookForm: FormGroup;
  searchForm: FormGroup;
  maxRating = 5;
  minRating = 0;
  defaultCount = 1;
  defaultImageUrl =
    'http://books.google.com/books/content?id=W0MEozflZjEC&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api';
  destroy$ = new Subject();
  categories: any[] = [];

  constructor(
    private menuService: MenuService,
    private activatedRoute: ActivatedRoute,
    private service: AdminService,
    private sharedService: SharedService,
    private notificationService: NotificationService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.data.type;
    this.menuService.changeSelectedMenu(this.type);
    const isbn = this.activatedRoute.snapshot.params.isbn;

    this.getCategories();

    if (this.type === 'add') {
      this.title = 'Add Book';
    } else {
      this.title = 'Update / Delete Books';
    }

    this.searchForm = this.formBuilder.group({
      isbn: [isbn, Validators.required]
    });

    this.createForm();

    this.type = this.activatedRoute.snapshot.data.type;
    if (this.type === 'add') {
      this.title = 'Add Book';
    } else {
      this.title = 'Update / Delete Books';
    }
  }

  /**
   * Creates the Book form
   */
  createForm() {
    this.bookForm = this.formBuilder.group({
      isbn: ['', Validators.required],
      title: ['', Validators.required],
      author: ['', Validators.required],
      description: ['', Validators.required],
      rating: [this.maxRating, [Validators.required, Validators.min(this.minRating), Validators.max(this.maxRating)]],
      count: [this.defaultCount, Validators.required],
      location: ['', Validators.required],
      imageUrl: [this.defaultImageUrl],
      category: [this.categories[0], Validators.required]
    });
  }

  /**
   * Find the book form the ISBN server
   */
  findByIsbn() {
    if (this.searchForm.valid) {
      this.showBookNotFound = false;
      const isbn = this.searchForm.get('isbn').value;
      this.isLoading = true;
      if (this.type === 'add') {
        this.service
          .getBook(isbn, false)
          .pipe(takeUntil(this.destroy$))
          .subscribe((book: Book) => {
            if (book === null) {
              this.service
                .getBookFromIsbnServer(isbn)
                .pipe(takeUntil(this.destroy$))
                .subscribe((result: any) => {
                  this.isLoading = false;
                  if (result && result.items && result.items.length > 0) {
                    const bookRef = result.items[0];
                    if (bookRef && bookRef.volumeInfo) {
                      this.bookForm.patchValue({
                        isbn,
                        title: bookRef.volumeInfo.title ? bookRef.volumeInfo.title : '',
                        author: bookRef.volumeInfo.authors ? (bookRef.volumeInfo.authors as []).toString() : '',
                        description: bookRef.volumeInfo.description ? bookRef.volumeInfo.description : '',
                        rating: bookRef.volumeInfo.averageRating ? bookRef.volumeInfo.averageRating : this.maxRating,
                        imageUrl:
                          bookRef.volumeInfo.imageLinks && bookRef.volumeInfo.imageLinks.smallThumbnail
                            ? bookRef.volumeInfo.imageLinks.smallThumbnail
                            : this.defaultImageUrl
                      });
                    }

                    this.notificationService.show('Successfully fetched ' + isbn + ' from ISBN server.');
                  } else {
                    this.bookForm.patchValue({
                      isbn
                    });
                    this.showBookDetailsNotFound = true;
                  }
                  this.showBook = true;
                });
            } else {
              this.isLoading = false;
              this.showBookExists = true;
            }
          });
      } else {
        this.service
          .getBook(isbn)
          .pipe(takeUntil(this.destroy$))
          .subscribe((book: Book) => {
            if (book != null) {
              this.bookForm.patchValue({
                isbn: book.isbn,
                title: book.title,
                author: book.author,
                description: book.description,
                rating: book.rating,
                imageUrl: book.imageUrl,
                count: book.count,
                location: book.location,
                category: book.category
              });
              this.showBook = true;
            } else {
              this.showBookNotFound = true;
            }
            this.isLoading = false;
          });
      }
    }
  }

  /**
   * Extract the form data
   */
  getFormData(data): Book {
    return {
      id: data.isbn,
      isbn: data.isbn,
      title: data.title,
      author: data.author,
      description: data.description,
      rating: data.rating,
      count: data.count,
      availableCount: data.count,
      location: data.location,
      imageUrl: data.imageUrl,
      category: data.category
    };
  }

  /**
   * Adds  the book to the library
   */
  addBook() {
    if (this.bookForm.valid) {
      this.isLoading = true;
      this.service
        .addBook(this.getFormData(this.bookForm.value))
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          if (data) {
            this.isLoading = false;
            this.notificationService.show(
              'Successfully added book' + this.bookForm.value.title + ' ( ' + this.bookForm.value.isbn + ' ).'
            );
            this.clearBookInfo();
          }
        });
    }
  }

  /**
   *  Fetch the categories
   */
  getCategories() {
    this.isLoading = true;
    this.sharedService
      .getCategories()
      .pipe(takeUntil(this.destroy$))
      .subscribe((categories: any[]) => {
        if (categories) {
          this.categories = categories;
          this.bookForm.patchValue({
            category: categories[0]
          });
          this.isLoading = false;
        }
      });
  }

  /**
   * Updates the boo information
   */
  updateBook() {
    if (this.bookForm.valid) {
      this.isLoading = true;
      this.service
        .updateBook(this.getFormData(this.bookForm.value))
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          if (data) {
            this.isLoading = false;
            this.notificationService.show(
              'Successfully updated ' + this.bookForm.value.title + ' ( ' + this.bookForm.value.isbn + ' ).'
            );
            this.clearBookInfo();
          }
        });
    }
  }

  /**
   * Delete the book
   */
  deleteBook() {
    const bookFormValue = this.bookForm.value;
    this.isLoading = true;
    this.service
      .deleteBook(bookFormValue.isbn)
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.isLoading = false;
          this.notificationService.show(
            'Successfully deleted ' + this.bookForm.value.title + ' ( ' + this.bookForm.value.isbn + ' ) .'
          );
          this.clearBookInfo();
        }
      });
  }

  clearBookInfo() {
    this.showBook = false;
    this.showBookNotFound = false;
    this.showBookDetailsNotFound = false;
    this.showBookExists = false;
    this.createForm();
  }

  changeTo(newPath: string) {
    this.router.navigate(['..', newPath, this.searchForm.value.isbn], { relativeTo: this.activatedRoute });
  }
  
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
