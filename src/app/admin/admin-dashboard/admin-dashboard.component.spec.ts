import { By } from '@angular/platform-browser';
import { MenuService } from './../../shared/menu/menu.service';
import { AdminMenu } from './../admin.menu';
import { MatDialog } from '@angular/material/dialog';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { AdminDashboardComponent } from './admin-dashboard.component';
import { NO_ERRORS_SCHEMA, Directive, Input, HostListener } from '@angular/core';
import { Menu } from 'src/app/shared/menu/menu.model';

/**
 * Admin Dasboard testing
 */
describe('Admin -> AdminDashboardComponent', () => {
  let component: AdminDashboardComponent;
  let fixture: ComponentFixture<AdminDashboardComponent>;
  let mockMatDialog;
  let menuService: MenuService;

  @Directive({
    // tslint:disable-next-line: directive-selector
    selector: '[routerLink]'
  })
  class RouterLinkDirectiveStub {
    // tslint:disable-next-line: no-input-rename
    @Input('routerLink') linkParams: any;
    navigatedTo: any = null;

    @HostListener('click')
    onClick() {
      this.navigatedTo = this.linkParams;
    }
  }

  beforeEach(async(() => {
    mockMatDialog = jasmine.createSpyObj(['open']);
    TestBed.configureTestingModule({
      declarations: [AdminDashboardComponent, RouterLinkDirectiveStub],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: MatDialog,
          useValue: mockMatDialog
        }
      ]
    }).compileComponents();
    menuService = TestBed.get(MenuService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load menus properly form menu service', fakeAsync(() => {
    const menu: Menu[] = AdminMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    const expected = menu;

    const actual = component.menu;

    expect(actual).toEqual(expected);
  }));

  it('should open dailog in  openConfigurationDialog', () => {
    component.openConfigurationDialog();
    expect(mockMatDialog.open).toHaveBeenCalled();
  });

  it('should render Dashboard on the Header', () => {
    const expected = 'Dashboard';
    const actual = fixture.debugElement.query(By.css('.mat-display-1')).nativeElement.textContent;
    expect(actual).toContain(expected);
  });

  it('should render correct number of the menu', fakeAsync(() => {
    const menu: Menu[] = AdminMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    fixture.detectChanges();
    const expected = AdminMenu.length;
    const actual = fixture.debugElement.queryAll(By.css('.menu__card')).length;
    expect(actual).toEqual(expected);
  }));

  it('should call  openConfigurationDialog on click of profile button', fakeAsync(() => {
    spyOn(component, 'openConfigurationDialog');

    fixture.detectChanges();
    fixture.debugElement.query(By.css('.config__button')).nativeElement.click();
    expect(component.openConfigurationDialog).toHaveBeenCalled();
  }));

  it(`it should have correct route for menus`, fakeAsync(() => {
    const menu: Menu[] = AdminMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    fixture.detectChanges();

    const routerLink = fixture.debugElement
      .query(By.directive(RouterLinkDirectiveStub))
      .injector.get(RouterLinkDirectiveStub);

    fixture.debugElement.query(By.css('.menu__card')).nativeElement.click();

    const expected = ['../', AdminMenu[0].path];
    const actual = routerLink.navigatedTo;
    expect(expected).toEqual(actual);
  }));
});
