import { AdminConfigurationComponent } from './../admin-configuration/admin-configuration.component';
import { MenuReference } from '../../shared/menu/menu.service';
import { Menu } from '../../shared/menu/menu.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuService } from 'src/app/shared/menu/menu.service';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

/**
 * Admin Dashboard 
 * Dashboard with Menu ites of the admin
 * Link to open the Configuration popup
 */

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit, OnDestroy {
  menu: Menu[];
  destroy$ = new Subject();

  constructor(private menuService: MenuService, private dialog: MatDialog) {}
  ngOnInit() {
    this.menuService.menu$.pipe(takeUntil(this.destroy$)).subscribe((menuRef: MenuReference) => {
      if (menuRef != null) {
        this.menu = menuRef.menu;
      }
    });

    this.menuService.changeSelectedMenu('');
  }

  /**
   * Opens the Configuration popup
   */
  openConfigurationDialog(): void {
    const dialogRef = this.dialog.open(AdminConfigurationComponent, {
      width: '500px',
      autoFocus: false
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
