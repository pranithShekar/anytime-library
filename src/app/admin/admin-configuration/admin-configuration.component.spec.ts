import { By } from '@angular/platform-browser';
import { NotificationService } from './../../shared/notification.service';
import { AdminService } from './../admin.service';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { AdminConfigurationComponent } from './admin-configuration.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

/**
 * Admin configuration Testing
 */

describe('Admin -> AdminConfigurationComponent', () => {
  let component: AdminConfigurationComponent;
  let fixture: ComponentFixture<AdminConfigurationComponent>;
  let mockAdminService;
  let mockNotificationService: NotificationService;
  const config = { issueDays: 1, renewDays: 1, maxNoOfBooksPerUser: 1 };

  beforeEach(async(() => {
    mockAdminService = jasmine.createSpyObj(['getConfig', 'updateConfig']);
    mockAdminService.getConfig.and.returnValue(of(config));
    mockNotificationService = jasmine.createSpyObj(['show']);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), ReactiveFormsModule],
      declarations: [AdminConfigurationComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: AdminService,
          useValue: mockAdminService
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService
        },
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {}
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getConfig  while loading', fakeAsync(() => {
    mockAdminService.getConfig.and.returnValue(of(config));
    tick(250);
    expect(mockAdminService.getConfig).toHaveBeenCalled();
  }));

  it('should call getConfig and load config while loading', fakeAsync(() => {
    tick(250);

    expect(component.config).toEqual(config);
  }));

  it('should call reset the form data on reset ', fakeAsync(() => {
    tick(250);

    component.reset();

    expect(component.configForm.value).toEqual(config);
  }));

  it('should call updateConfig service with proper config', () => {
    mockAdminService.updateConfig.and.returnValue(of(config));
    component.update();
    expect(mockAdminService.updateConfig).toHaveBeenCalledWith(config);
  });

  it('should call notification service after update success', () => {
    mockAdminService.updateConfig.and.returnValue(of(config));

    component.update();

    expect(mockNotificationService.show).toHaveBeenCalled();
  });

  it('should not call updateConfig if form is invalid ', () => {
    mockAdminService.updateConfig.and.returnValue(of(config));
    component.configForm.patchValue({
      issueDays: null
    });
    component.update();
    expect(mockAdminService.updateConfig).not.toHaveBeenCalled();
  });

  it('should not call notification service after update fail', () => {
    mockAdminService.updateConfig.and.returnValue(of(null));

    component.update();

    expect(mockNotificationService.show).not.toHaveBeenCalled();
  });

  it('should not call updateConfig onclick of the update button ', () => {
    const updateSpy = spyOn(component, 'update').and.callFake(() => {});

    fixture.debugElement.query(By.css('.update__button')).nativeElement.click();

    expect(updateSpy).toHaveBeenCalled();
  });
});
