import { AdminService } from './../admin.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { updateLocale } from 'moment';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotificationService } from 'src/app/shared/notification.service';
import { MatDialogRef } from '@angular/material/dialog';


/**
 * Admin Configuration
 * View and Update Configurations
 */
@Component({
  selector: 'app-admin-configuration',
  templateUrl: './admin-configuration.component.html',
  styleUrls: ['./admin-configuration.component.scss']
})
export class AdminConfigurationComponent implements OnDestroy {
  configForm: FormGroup;
  destroy$ = new Subject();
  config: { issueDays: number; renewDays: number; maxNoOfBooksPerUser: number };
  constructor(
    private formBuilder: FormBuilder,
    private service: AdminService,
    private notificationService: NotificationService,
    private dialogRef: MatDialogRef<AdminConfigurationComponent>
  ) {
    const validators = [Validators.required, Validators.min(1)];
    this.configForm = this.formBuilder.group({
      issueDays: [6, validators],
      renewDays: [5, validators],
      maxNoOfBooksPerUser: [4, validators]
    });

    this.getConfig();
  }

  /**
   * Get the Admin configuration
   */
  getConfig() {
    this.service
      .getConfig()
      .pipe(takeUntil(this.destroy$))
      .subscribe((config: { issueDays: number; renewDays: number; maxNoOfBooksPerUser: number }) => {
        if (config) {
          this.config = config;
          this.configForm.patchValue(config);
        }
      });
  }

   /**
   * Reset the modified configuration
   */
  reset(): void {
    this.configForm.patchValue(this.config);
  }

   /**
   * Update the Admin configuration
   */
  update() {
    if (this.configForm.valid) {
      this.config = this.configForm.value;
      this.service
        .updateConfig(this.config)
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          if (data) {
            this.notificationService.show('Configuration Updated.');
            this.dialogRef.close();
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
