import { ReviewFilterPipe } from './review-filter.pipe';
import { Review } from './model/review.model';

/**
 * Review Filter Service
 */
describe('Shared -> ReviewFilterPipe', () => {
  const review: Review[] = [
    {
      id: '122',
      bookId: '122',
      title: 'abc',
      date: '12-Aug-2018',
      rating: 3,
      review: 'Good',
      issuedId: '1233',
      userName: 'Abc'
    },
    {
      id: '122',
      bookId: '122',
      title: 'abc',
      date: '12-Aug-2018',
      rating: 3,
      review: 'Good nice',
      issuedId: '1233',
      userName: 'Abc'
    },
    {
      id: '122',
      bookId: '122',
      title: 'abc',
      date: '12-Aug-2018',
      rating: 3,
      review: 'Nice',
      issuedId: '1233',
      userName: 'Abc'
    }
  ];
  it('create an instance', () => {
    const pipe = new ReviewFilterPipe();
    expect(pipe).toBeTruthy();
  });

  it('should display only based on search value', () => {
    const pipe = new ReviewFilterPipe();

    const expected = 2;
    const actual = pipe.transform(review, 'good');

    expect(actual.length).toBe(expected);
  });

  it('should display all if no  search value', () => {
    const pipe = new ReviewFilterPipe();

    const expected = 3;
    const actual = pipe.transform(review);

    expect(actual.length).toBe(expected);
  });
});
