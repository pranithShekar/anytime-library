/**
 * Review model
 */
export class Review {
  id: string;
  issuedId: string;
  bookId: string;
  title: string;
  review: string;
  date: string;
  userName: string;
  rating: number;
}
