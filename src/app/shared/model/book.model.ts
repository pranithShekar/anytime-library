/**
 * Book model
 */

export class Book {
  id: string;
  isbn: string;
  title: string;
  author: string;
  description: string;
  rating: number;
  count: number;
  availableCount: number;
  location: string;
  imageUrl: string;
  category: string;
}

/**
 * IssuedBook model
 */

export class IssuedBook {
  id: string;
  isbn: string;
  title: string;
  author: string;
  imageUrl: string;
  userId: string;
  userName: string;
  issuedDate: string;
  returnDate: string;
  location: string;
  status: string;
}
