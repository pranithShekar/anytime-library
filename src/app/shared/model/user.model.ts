import { UserRoleEnum } from '../atl.enum';

/**
 * Use model
 */
export class User {
  userId: string;
  name: string;
  email: string;
  imageUrl: string;
  role: UserRoleEnum;
}
