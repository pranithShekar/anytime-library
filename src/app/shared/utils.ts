import { Moment } from 'moment';
import { Observable, of } from 'rxjs';

/**
 * Utility function
 */

/**
 * Date format 
 * @param date
 * @return string
 */
export function dateFormat(date: Moment) {
  if (date) {
    return date.format('DD-MMM-YYYY');
  } else {
    return null;
  }
}

/**
 * Log service
 */
export function log(message: string) {
  console.log(message);
}
