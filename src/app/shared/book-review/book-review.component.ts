import { NotificationService } from './../notification.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Review } from './../model/review.model';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from '../shared.service';
import { takeUntil } from 'rxjs/operators';
import { dateFormat } from '../utils';
import * as moment from 'moment';

export interface DialogData {
  mode: ReviewMode;
  issuedId: string;
  bookId: string;
}

export enum ReviewMode {
  VIEW,
  EDIT
}

/**
 * Book Review Component
 */
@Component({
  selector: 'app-book-review',
  templateUrl: './book-review.component.html',
  styleUrls: ['./book-review.component.scss']
})
export class BookReviewComponent implements OnInit, OnDestroy {
  destroy$ = new Subject();
  reviews: Review[] = [];
  issuedId: string;
  bookId: string;
  maxRating = 5;
  minRating = 1;
  reviewForm: FormGroup;
  isUpdate: boolean;
  reviewId: string;
  searchValue: string;
  constructor(
    private service: SharedService,
    private dialogRef: MatDialogRef<BookReviewComponent>,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,

    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.bookId = data.bookId;
    this.issuedId = data.issuedId;

    this.reviewForm = this.formBuilder.group({
      title: ['', Validators.required],
      review: ['', Validators.required],
      rating: [this.maxRating, [Validators.required, Validators.min(this.minRating), Validators.max(this.maxRating)]]
    });

    if (!this.isEditMode()) {
      this.service
        .getReviews(this.bookId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((reviews: Review[]) => {
          if (reviews) {
            this.reviews = reviews;
          }
        });
    } else {
      this.service
        .getReview(this.issuedId)
        .pipe(takeUntil(this.destroy$))
        .subscribe((reviews: Review[]) => {
          if (reviews == null) {
            this.notificationService.show('Something went wrong. Try again.');
          } else {
            if (reviews.length > 0) {
              this.isUpdate = true;
              this.reviewId = reviews[0].id;
              this.reviewForm.patchValue({
                title: reviews[0].title,
                review: reviews[0].review,
                rating: reviews[0].rating
              });
            }
          }
        });
    }
  }

  ngOnInit() {}

  /**
   * Check whether it is edit mode or not
   */
  isEditMode(): boolean {
    return this.data.mode === ReviewMode.EDIT;
  }

  /**
   * Submit the review
   */
  submitReview() {
    if (this.reviewForm.valid) {
      const review = {
        id: '',
        issuedId: this.issuedId,
        bookId: this.bookId,
        title: this.reviewForm.value.title,
        review: this.reviewForm.value.review,
        date: dateFormat(moment()),
        userName: 'Rakesh',
        rating: this.reviewForm.value.rating
      };
      if (!this.isUpdate) {
        this.service
          .addReview(review)
          .pipe(takeUntil(this.destroy$))
          .subscribe(data => {
            if (data) {
              this.dialogRef.close();
              this.notificationService.show('Added Review Successfully.');
            }
          });
      } else {
        this.service
          .updateReview({ ...review, id: this.reviewId })
          .pipe(takeUntil(this.destroy$))
          .subscribe(data => {
            if (data) {
              this.dialogRef.close();
              this.notificationService.show('Updated Review Successfully.');
            }
          });
      }
    } else {
      this.reviewForm.markAllAsTouched();
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
