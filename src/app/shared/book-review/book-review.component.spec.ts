import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificationService } from './../notification.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookReviewComponent, ReviewMode } from './book-review.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReviewFilterPipe } from '../review-filter.pipe';
import { SharedService } from '../shared.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { of } from 'rxjs';


/**
 *  Book Review Component Testing
 */
describe('BookReviewComponent', () => {
  let component: BookReviewComponent;
  let fixture: ComponentFixture<BookReviewComponent>;
  let mockSharedService;
  let mockNotificationService;

  beforeEach(async(() => {
    mockSharedService = jasmine.createSpyObj(['getReviews', 'addReview']);
    mockSharedService.getReviews.and.returnValue(of([true]));
    mockNotificationService = jasmine.createSpyObj(['show']);

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule],
      declarations: [BookReviewComponent, ReviewFilterPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: SharedService,
          useValue: mockSharedService
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService
        },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {}
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
