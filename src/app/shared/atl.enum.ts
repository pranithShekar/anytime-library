/**
 * Application Enums
 */
export enum UserRoleEnum {
  USER = 'User',
  ADMIN = 'Admin'
}

export enum IssuedStatusEnum {
  ISSUED = 'Issued',
  RETURNED = 'Returned',
  OVERDUE = 'Overdue',
  RENEWED = 'Renewed'
}
