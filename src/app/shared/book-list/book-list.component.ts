import { NotificationService } from './../notification.service';
import { IssuedBook } from './../model/book.model';
import { SharedService } from './../shared.service';
import { log } from 'util';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { MenuService } from 'src/app/shared/menu/menu.service';
import { Sort } from '@angular/material/sort';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Book } from '../model/book.model';
import { dateFormat } from '../utils';
import * as moment from 'moment';
import { UserRoleEnum, IssuedStatusEnum } from '../atl.enum';
import { MatDialog } from '@angular/material/dialog';
import { BookReviewComponent, ReviewMode } from '../book-review/book-review.component';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { User } from '../model/user.model';

/**
 * Book List Component
 * User can view the book info 
 * can filter the books or search
 * if Available he can issue it himself
 */
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit, OnDestroy {
  type: string;
  role: string;
  roleRef = UserRoleEnum;
  showBooks = false;
  books: Book[];
  destroy$ = new Subject();
  pageSize = 5;
  pageIndex = 1;
  sortOrder: string;
  sortBy: string;
  hasSort = false;
  booksTotalCount = 0;
  booksForm: FormGroup;
  categories: any[] = [];
  isLoading = false;
  issueDays: number;
  maxNoOfBooksPerUser: number;
  user: User;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild('searchBox', { static: true }) searchBox: ElementRef;

  constructor(
    private menuService: MenuService,
    private activatedRoute: ActivatedRoute,
    private service: SharedService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService,
    private dialog: MatDialog,
    private authenticationService: AuthenticationService
  ) {
    this.booksForm = this.formBuilder.group({
      searchValue: [''],
      category: ['']
    });
    this.authenticationService.user$.pipe(takeUntil(this.destroy$)).subscribe((user: User) => {
      if (user) {
        this.user = user;
      }
    });
  }

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.data.type;
    this.role = this.activatedRoute.snapshot.data.role;
    const isbn = this.activatedRoute.snapshot.params.isbn;
    if (isbn) {
      this.booksForm.patchValue({
        searchValue: isbn
      });
    }

    this.menuService.changeSelectedMenu(this.type);
    this.getCategories();

    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        this.searchEvent();
      });

    this.getConfig();
    this.getBooksCount();
    this.getBooks();
  }
/**
 * Handles Search Events
 */
  searchEvent() {
    const category = this.booksForm.get('category').value;
    const searchValue = this.booksForm.get('searchValue').value;
    if (searchValue === '' && category === '') {
      this.clearAll();
    } else {
      this.search();
    }
  }
  
  /**
   * Opens the Book Review Dailog
   * @param book
   */
  openBookReviewDialog(book: IssuedBook): void {
    const dialogRef = this.dialog.open(BookReviewComponent, {
      width: '600px',
      autoFocus: false,
      data: {
        mode: ReviewMode.VIEW,
        bookId: book.isbn
      }
    });
  }

  /**
   * Sort the books
   * @param book
   */
  sortBooks(sort: Sort) {
    this.sortOrder = sort.direction;
    this.sortBy = sort.active;
    if (this.sortOrder) {
      this.hasSort = true;
    } else {
      this.hasSort = true;
    }
    this.resetPagination();
  }

  /** Paginate the list
   * @param pageEvent
   */
  paginate(pageEvent: PageEvent) {
    this.pageSize = pageEvent.pageSize;
    this.pageIndex = pageEvent.pageIndex + 1;
    this.getBooks();
  }

  /**
   * Get config
   */
  getConfig() {
    this.service
      .getConfig()
      .pipe(takeUntil(this.destroy$))
      .subscribe((config: { issueDays: number; renewDays: number; maxNoOfBooksPerUser: number }) => {
        if (config) {
          this.issueDays = config.issueDays;
          this.maxNoOfBooksPerUser = config.maxNoOfBooksPerUser;
        }
      });
  }


  /**
   * Get books count
   */
  getBooksCount() {
    this.service
      .getBooksCount(this.booksForm.get('searchValue').value, this.booksForm.get('category').value)
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        if (count) {
          this.booksTotalCount = count;
        }
      });
  }

  /**
   * Get Books
   */
  getBooks() {
    this.isLoading = true;

    this.service
      .getBooks(
        this.pageIndex,
        this.pageSize,
        this.booksForm.get('searchValue').value,
        this.booksForm.get('category').value,
        this.hasSort,
        this.sortBy,
        this.sortOrder
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((books: Book[]) => {
        if (books) {
          this.isLoading = false;
          this.books = books;
          this.showBooks = true;
        }
      });
  }

  /**
   * Get Categories
   */
  getCategories() {
    this.isLoading = true;
    this.service
      .getCategories()
      .pipe(takeUntil(this.destroy$))
      .subscribe((categories: any[]) => {
        if (categories) {
          this.categories = categories;
          this.isLoading = false;
        }
      });
  }

  /**
   * Search
   */
  search() {
    this.getBooksCount();
    this.resetPagination();
  }

  clearSearch() {
    this.booksForm.patchValue({ searchValue: '' });
    this.resetPagination();
  }

  clearCategory(event) {
    this.booksForm.patchValue({ category: '' });
    this.resetPagination();
    if (event) {
    }
  }

  clearAll() {
    this.clearSearch();
    this.clearCategory(null);
  }

  resetPagination() {
    this.getBooksCount();
    if (this.paginator && this.paginator.hasPreviousPage()) {
      this.paginator.firstPage();
    } else {
      this.getBooks();
    }
  }

  /**
   * Issue book
   * @param book
   */
  issueBook(book: Book) {
    const issueBook: IssuedBook = {
      id: '',
      isbn: book.isbn,
      title: book.title,
      author: book.author,
      imageUrl: book.imageUrl,
      userId: this.user.userId,
      userName: this.user.name,
      issuedDate: dateFormat(moment()),
      returnDate: dateFormat(moment().add(this.issueDays, 'days')),
      status: IssuedStatusEnum.ISSUED,
      location: book.location
    };

    this.service
      .getIssuedBooksCountByUser(this.user.userId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((books: IssuedBook[]) => {
        if (books) {
          if (this.maxNoOfBooksPerUser < books.length) {
            this.notificationService.show('You have exceeded maximum issue limit.');
          } else {
            this.service
              .issueBook(issueBook)
              .pipe(takeUntil(this.destroy$))
              .subscribe(() => {
                this.service
                  .updateBook(book.id, { availableCount: book.availableCount - 1 })
                  .pipe(takeUntil(this.destroy$))
                  .subscribe(data => {
                    if (data) {
                      this.getBooks();
                      this.notificationService.show(
                        'Successfully Issued ' +
                          issueBook.isbn +
                          ' . Please find the book at location ' +
                          issueBook.location
                      );
                    }
                  });
              });
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
