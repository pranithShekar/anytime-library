import { MatSelectModule } from '@angular/material/select';
import { AuthenticationService } from './../../core/authentication.service';
import { UserRoleEnum } from './../atl.enum';
import { RouterTestingModule } from '@angular/router/testing';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatFormField, MatFormFieldModule, MatInputModule } from '@angular/material';
import { NotificationService } from './../notification.service';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { IssuedBookListComponent } from './issued-book-list.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReviewFilterPipe } from '../review-filter.pipe';
import { SharedService } from '../shared.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IssuedBook, Book } from '../model/book.model';

describe('IssuedBookListComponent', () => {
  let component: IssuedBookListComponent;
  let fixture: ComponentFixture<IssuedBookListComponent>;
  let mockSharedService;
  let mockNotificationService;
  let authenticationService;
  const user = {
    email: 'abc@gmail.com',
    name: 'ABC',
    imageUrl: '/abc.jpeg',
    role: UserRoleEnum.USER,
    userId: '123'
  };

  const book: Book = {
    id: '1111',
    isbn: '1111',
    title: 'title',
    author: 'author',
    description: 'description',
    rating: 1,
    count: 3,
    availableCount: 3,
    location: 'location',
    imageUrl: 'imageUrl',
    category: 'category'
  };

  const issuedBook: IssuedBook = {
    id: '1111',
    isbn: '1111',
    title: 'title',
    author: 'author',
    location: 'location',
    imageUrl: 'imageUrl',
    userId: '123',
    userName: 'Anc',
    issuedDate: '12-Aug-2019',
    returnDate: '22-Aug-2019',
    status: 'issued'
  };

  beforeEach(async(() => {
    mockSharedService = jasmine.createSpyObj([
      'getConfig',
      'getIssuedBooksCount',
      'getIssuedBooks',
      'updateIssuedBook',
      'getBook',
      'updateBook'
    ]);
    mockNotificationService = jasmine.createSpyObj(['show']);
    mockSharedService.getConfig.and.returnValue(of([true]));
    mockSharedService.getIssuedBooksCount.and.returnValue(of([true]));
    mockSharedService.getIssuedBooks.and.returnValue(of([issuedBook]));
    mockSharedService.updateIssuedBook.and.returnValue(of(issuedBook));
    mockSharedService.getBook.and.returnValue(of(book));
    mockSharedService.updateBook.and.returnValue(of([book]));

    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        RouterTestingModule.withRoutes([]),
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule
      ],
      declarations: [IssuedBookListComponent, ReviewFilterPipe],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: SharedService,
          useValue: mockSharedService
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              data: {
                type: 'issue',
                role: UserRoleEnum.USER
              }
            }
          }
        },
        {
          provide: MatDialog,
          useValue: null
        },

        { provide: MAT_DIALOG_DATA, useValue: {} },
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {}
          }
        }
      ]
    }).compileComponents();

    authenticationService = TestBed.get(AuthenticationService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuedBookListComponent);
    component = fixture.componentInstance;
    component.user = user;
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    expect(component).toBeTruthy();
  }));

  it('should call getBooks onsearch  ', fakeAsync(() => {
    spyOn(component, 'getBooks');
    component.searchEvent();
    tick(250);
    expect(component.getBooks).toHaveBeenCalled();
  }));

  it('should call getBooks on clear of all search params', fakeAsync(() => {
    spyOn(component, 'getBooks');
    component.clearAll();
    tick(250);
    expect(component.getBooks).toHaveBeenCalled();
  }));

  it('should call getbooks on clear of all search params', fakeAsync(() => {
    spyOn(component, 'getBooks');
    component.sortBooks({ direction: 'asc', active: 'a' });
    tick(250);
    expect(component.getBooks).toHaveBeenCalled();
  }));

  it('should call getBooks on sortSet', fakeAsync(() => {
    spyOn(component, 'getBooks');
    component.sortBooks({ direction: 'asc', active: 'a' });
    tick(250);
    expect(component.getBooks).toHaveBeenCalled();
  }));

  it('should call getBooks on pagination', fakeAsync(() => {
    spyOn(component, 'getBooks');
    component.paginate({ pageIndex: 1, pageSize: 10, length: 10 });
    tick(250);
    expect(component.getBooks).toHaveBeenCalled();
  }));

  it('should call returnBook service on returnbook', fakeAsync(() => {
    component.returnBook(issuedBook);
    tick(250);
    expect(mockSharedService.updateBook).toHaveBeenCalled();
  }));

  it('should call renew service on returnbook', fakeAsync(() => {
    component.renewBook(issuedBook);
    tick(250);
    expect(mockSharedService.updateIssuedBook).toHaveBeenCalled();
  }));
});
