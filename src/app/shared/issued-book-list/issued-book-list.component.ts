import { User } from './../model/user.model';
import { ReviewMode } from './../book-review/book-review.component';
import { Book } from './../model/book.model';
import { NotificationService } from './../notification.service';
import { IssuedStatusEnum } from './../atl.enum';
import { SharedService } from './../shared.service';
import { IssuedBook } from '../model/book.model';
import { AdminService } from '../../admin/admin.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MenuService } from 'src/app/shared/menu/menu.service';
import { Sort } from '@angular/material/sort';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { dateFormat } from 'src/app/shared/utils';
import { UserRoleEnum } from '../atl.enum';
import { Moment } from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { BookReviewComponent } from '../book-review/book-review.component';
import { AuthenticationService } from 'src/app/core/authentication.service';

export const DATE_FORMATS = {
  parse: {
    dateInput: 'DD-MMM-YYYY'
  },
  display: {
    dateInput: 'DD-MMM-YYYY',
    monthYearLabel: 'MMM-YYYY',
    dateA11yLabel: 'DD-MMM-YYYY',
    monthYearA11yLabel: 'DD-MMM-YYYY'
  }
};

/**
 * Issued Book List Component
 * To check the status of the book
 * To return or renew the book
 * Link to open the review form
 */
@Component({
  selector: 'app-issued-book-list',
  templateUrl: './issued-book-list.component.html',
  styleUrls: ['./issued-book-list.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS }
  ]
})
export class IssuedBookListComponent implements OnInit, AfterViewInit, OnDestroy {
  type: string;
  role: string;
  roleRef = UserRoleEnum;
  issuedStatusRef = IssuedStatusEnum;
  showBooks = false;
  books: IssuedBook[];
  destroy$ = new Subject();
  pageSize = 4;
  pageIndex = 1;
  sortOrder: string;
  sortBy: string;
  hasSort = false;
  booksTotalCount = 0;
  booksForm: FormGroup;
  statusList = ['Issued', 'Returned', 'Overdue', 'Renewed'];
  isLoading = false;
  renewDays: number;
  user: User;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild('searchBox', { static: false }) searchBox: ElementRef;
  @ViewChild('userIdInput', { static: false }) userIdInput: ElementRef;

  constructor(
    private menuService: MenuService,
    private activatedRoute: ActivatedRoute,
    private service: SharedService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private authenticationService: AuthenticationService
  ) {
    this.booksForm = this.formBuilder.group({
      searchValue: [''],
      userId: [''],
      issuedDate: [''],
      status: ['']
    });
    this.authenticationService.user$.pipe(takeUntil(this.destroy$)).subscribe((user: User) => {
      if (user) {
        this.user = user;
      }
    });
  }

  ngOnInit() {
    // this.openBookReviewDialog(null);
    this.type = this.activatedRoute.snapshot.data.type;
    this.role = this.activatedRoute.snapshot.data.role;
    this.menuService.changeSelectedMenu(this.type);

    if (this.role === UserRoleEnum.USER) {
      this.booksForm.patchValue({ userId: this.user.userId });
    }

    this.sortOrder = 'desc';
    this.sortBy = 'issuedDate';
    this.hasSort = true;

    this.getConfig();
    this.getBooksCount();
    this.getBooks();
  }

  ngAfterViewInit() {
    fromEvent(this.searchBox.nativeElement, 'input')
      .pipe(
        debounceTime(1000),
        takeUntil(this.destroy$)
      )
      .subscribe((event: any) => {
        this.searchEvent();
      });

    if (this.role === UserRoleEnum.ADMIN) {
      fromEvent(this.userIdInput.nativeElement, 'input')
        .pipe(
          debounceTime(1000),
          takeUntil(this.destroy$)
        )
        .subscribe((event: any) => {
          this.searchEvent();
        });
    }
  }

  /**
   * Open Book review Dailog
   */
  openBookReviewDialog(issuedBook: IssuedBook): void {
    const dialogRef = this.dialog.open(BookReviewComponent, {
      width: '600px',
      autoFocus: false,
      data: {
        mode: ReviewMode.EDIT,
        issuedId: issuedBook.id,
        bookId: issuedBook.isbn
      }
    });
  }

  /**
   * Get Configs
   */
  getConfig() {
    this.service
      .getConfig()
      .pipe(takeUntil(this.destroy$))
      .subscribe((config: { issueDays: number; renewDays: number; maxNoOfBooksPerUser: number }) => {
        if (config) {
          this.renewDays = config.renewDays;
        }
      });
  }


  /**
   * Search Events
   */
  searchEvent() {
    const userId = this.booksForm.get('userId').value;
    const searchValue = this.booksForm.get('searchValue').value;
    const issuedDate = this.booksForm.get('issuedDate').value;
    const status = this.booksForm.get('status').value;

    if (searchValue === '' && userId === '' && issuedDate === '' && status === '') {
      this.clearAll();
    } else {
      this.search();
    }
  }

  /**
   * Sort books
   */
  sortBooks(sort: Sort) {
    this.sortOrder = sort.direction;
    this.sortBy = sort.active;
    if (this.sortOrder) {
      this.hasSort = true;
    } else {
      this.hasSort = true;
    }
    this.resetPagination();
  }

  /**
   * Paginate
   */
  paginate(pageEvent: PageEvent) {
    this.pageSize = pageEvent.pageSize;
    this.pageIndex = pageEvent.pageIndex + 1;
    this.getBooks();
  }

  /**
   * Get Books count
   */
  getBooksCount() {
    this.isLoading = true;
    this.service
      .getIssuedBooksCount(
        this.booksForm.get('userId').value,
        this.booksForm.get('searchValue').value,
        this.booksForm.get('status').value,
        dateFormat(this.booksForm.get('issuedDate').value)
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((count: number) => {
        if (count) {
          this.isLoading = false;
          this.booksTotalCount = count;
        }
      });
  }

  /**
   * Get Books
   */
  getBooks() {
    this.isLoading = true;
    this.service
      .getIssuedBooks(
        this.pageIndex,
        this.pageSize,
        this.booksForm.get('userId').value,
        this.booksForm.get('searchValue').value,
        this.booksForm.get('status').value,
        dateFormat(this.booksForm.get('issuedDate').value),
        this.hasSort,
        this.sortBy,
        this.sortOrder
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe((books: IssuedBook[]) => {
        if (books) {
          this.books = books;
          this.showBooks = true;
          this.isLoading = false;
        }
      });
  }

  /**
   * Search
   */
  search() {
    this.getBooksCount();
    this.resetPagination();
  }

  clearSearch() {
    this.booksForm.patchValue({ searchValue: '' });
    this.resetPagination();
  }

  clearUser() {
    this.booksForm.patchValue({ userId: '' });
    this.resetPagination();
  }

  clearIssuedDate() {
    this.booksForm.patchValue({ issuedDate: '' });
    this.resetPagination();
  }

  clearStatus(event: any) {
    this.booksForm.patchValue({ status: '' });
    this.resetPagination();
    if (event) {
      event.stopPropagation();
    }
  }

  clearAll() {
    this.clearSearch();
    this.clearUser();
    this.clearStatus(null);
    this.clearIssuedDate();
  }

  resetPagination() {
    this.getBooksCount();
    if (this.paginator && this.paginator.hasPreviousPage()) {
      this.paginator.firstPage();
    } else {
      this.getBooks();
    }
  }
  /**
   * Return book
   * @param book
   */
  returnBook(book: IssuedBook) {
    const issueBook: IssuedBook = {
      ...book,
      status: IssuedStatusEnum.RETURNED,
      returnDate: dateFormat(moment())
    };

    this.isLoading = true;
    this.service
      .updateIssuedBook(issueBook)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.service
          .getBook(book.isbn)
          .pipe(takeUntil(this.destroy$))
          .subscribe((bookResult: Book) => {
            this.service
              .updateBook(bookResult.id, { availableCount: bookResult.availableCount + 1 })
              .pipe(takeUntil(this.destroy$))
              .subscribe(data => {
                if (data) {
                  this.isLoading = false;
                  this.notificationService.show(
                    'Successfully Returned ' + issueBook.isbn + '. Please place book at location ' + issueBook.location
                  );
                  this.getBooks();
                }
              });
          });
      });
  }

  /**
   * Renew book
   * @param book
   */
  renewBook(book: IssuedBook) {
    const issueBook: IssuedBook = {
      ...book,
      status: IssuedStatusEnum.RENEWED,
      returnDate: dateFormat(moment().add(this.renewDays, 'days'))
    };

    this.isLoading = true;
    this.service
      .updateIssuedBook(issueBook)
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        if (data) {
          this.isLoading = false;
          this.notificationService.show(
            'Successfully Renewd ' + issueBook.isbn + ' for next ' + this.renewDays + ' days'
          );
          this.getBooks();
        }
      });
  }

  /**
   * Check for overdue
   */
  isOverDue(returnDate: Moment): boolean {
    return moment().diff(returnDate, 'day') > 0;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
