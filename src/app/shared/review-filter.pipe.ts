import { Pipe, PipeTransform } from '@angular/core';
import { Review } from './model/review.model';

/**
 * Review Filter 
 * Will be user to filter the review based on search inputs
 */
@Pipe({
  name: 'reviewFilter'
})
export class ReviewFilterPipe implements PipeTransform {
  transform(reviews: Review[], ...args: any[]): Review[] {
    if (args[0]) {
      return reviews.filter(review => review.review.toLowerCase().includes(args[0].toLowerCase()));
    } else {
      return reviews;
    }
  }
}
