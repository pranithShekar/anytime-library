import { By } from '@angular/platform-browser';
import { HighlighterDirective } from './highlighter.directive';
import { async, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';


/**
 * Highlighter Directive tetsing
 */
describe('HighlighterDirective', () => {
  @Component({
    // tslint:disable-next-line: component-selector
    selector: 'my-test-component',
    template: ''
  })
  class TestComponent {}
  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [TestComponent, HighlighterDirective]
    });
  });

  it('should create an instance', () => {
    const directive = new HighlighterDirective(null);
    expect(directive).toBeTruthy();
  });

  it('should be able to test directive', async(() => {
    TestBed.overrideComponent(TestComponent, {
      set: {
        template: '<div appHighlighter="red"></div>'
      }
    });

    TestBed.compileComponents().then(() => {
      const fixture = TestBed.createComponent(TestComponent);
      const directiveEl = fixture.debugElement.query(By.directive(HighlighterDirective));
      expect(directiveEl).not.toBeNull();

      // directiveEl.nativeElement.click();

      const directiveInstance = directiveEl.injector.get(HighlighterDirective);
      directiveInstance.onMouseEnter();
      directiveInstance.onMouseLeave();
      fixture.detectChanges();
      expect(directiveInstance.appHighlighter).toBe('red');
    });
  }));
});
