import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';
import { NotificationService } from './notification.service';
import { Observable, of } from 'rxjs';
import { log } from 'util';

@Injectable()
export class ErrorHandlerService {
  constructor(private notificationService: NotificationService) {}

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      log(`${operation} failed: ${error.message}`);
      this.notificationService.show('Something went wrong. Try again');
      return of(result as T);
    };
  }
}
