import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';

/**
 * Notification Service
 */
@Injectable()
export class NotificationService {
  constructor(private snackBar: MatSnackBar) {}

  show(messege: string): void {
    this.snackBar.open(messege, null, {
      duration: 4000,
      panelClass: 'notification__background'
    });
  }
}
