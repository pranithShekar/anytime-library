import { Directive, ElementRef, HostListener, Input } from '@angular/core';

/**
 * Highlighter Directive
 */
@Directive({
  selector: '[appHighlighter]'
})
export class HighlighterDirective {
  @Input() appHighlighter: string;
  constructor(private elementRef: ElementRef) {}
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight(this.appHighlighter);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }

  private highlight(color: string) {
    this.elementRef.nativeElement.style.color = color;
  }
}
