import { NotificationService } from './notification.service';
import { SharedService } from './shared.service';
import { SharedMaterialModule } from './shared.material';
import { BookListComponent } from './book-list/book-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IssuedBookListComponent } from './issued-book-list/issued-book-list.component';
import { BookReviewComponent } from './book-review/book-review.component';
import { ErrorHandlerService } from './errorHandler.service';
import { HighlighterDirective } from './highlighter.directive';
import { ReviewFilterPipe } from './review-filter.pipe';

/**
 * Shared Module
 */
@NgModule({
  declarations: [
    BookListComponent,
    IssuedBookListComponent,
    BookReviewComponent,
    HighlighterDirective,
    ReviewFilterPipe
  ],
  imports: [CommonModule, SharedMaterialModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  exports: [BookListComponent, IssuedBookListComponent],
  providers: [SharedService, NotificationService, ErrorHandlerService],
  entryComponents: [BookReviewComponent]
})
export class SharedModule {}
