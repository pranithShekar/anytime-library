import { dateFormat } from 'src/app/shared/utils';
import { ReviewFilterPipe } from './review-filter.pipe';
import { log } from './utils';
import * as moment from 'moment';

/**
 * Util Tetsing
 */
describe('Shared -> Utils', () => {
  it('Should print on the console - Log', () => {
    spyOn(console, 'log');
    log('Hello');
    expect(console.log).toHaveBeenCalled();
  });

  it('Should povide date in defined format', () => {
    const expected = '08-Dec-2019';
    const actual = dateFormat(moment('2019-12-08'));
    expect(actual).toEqual(expected);
  });

  it('Should povide null if undefined ', () => {
    const expected = null;
    const actual = dateFormat(null);
    expect(actual).toEqual(expected);
  });
});
