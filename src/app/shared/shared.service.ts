import { ErrorHandlerService } from './errorHandler.service';
import { Review } from './model/review.model';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { log } from 'util';
import { environment } from 'src/environments/environment.prod';
import { IssuedBook, Book } from './model/book.model';

/**
 * Shared service
 */
@Injectable()
export class SharedService {
  private url = environment.API_URL;
  private isbnUrl = environment.ISBN_URL;

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) {}

  /**
   * Get Config
   * @retun  Observable<any>
   */
  getConfig(): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.url + `/config`, options).pipe(
      tap(config => log(`fetched cofig`)),
      catchError(this.errorHandlerService.handleError('getConfig', null))
    );
  }

  /**
   * Get Categories
   * @param
   * @retun  Observable<any>
   */  
  getCategories(): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.url + `/categories`, options).pipe(
      tap(category => log(`fetched category`)),
      catchError(this.errorHandlerService.handleError('getCategory', null))
    );
  }

  /**
   * Get Reviews
   * @param bookId
   * @retun  Observable<any>
   */  
  getReviews(bookId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.url + `/reviews?bookId=${bookId}&_sort=date&_order=desc`, options).pipe(
      tap(category => log(`fetched category`)),
      catchError(this.errorHandlerService.handleError('getCategory', null))
    );
  }

  /**
   * Add Review
   * @param review
   * @retun  Observable<any>
   */
  addReview(review: Review): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.post(this.url + `/reviews`, review, options).pipe(
      tap(category => log(`added review`)),
      catchError(this.errorHandlerService.handleError('addReview', null))
    );
  }

  /**
   * Get Review
   * @param issuedId
   * @retun  Observable<any>
   */  
  getReview(issuedId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.url + `/reviews?issuedId=${issuedId}`, options).pipe(
      tap(category => log(`added review`)),
      catchError(this.errorHandlerService.handleError('addReview', null))
    );
  }

  /**
   * Update Review
   * @param review
   * @retun  Observable<any>
   */
  updateReview(review: Review): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.patch(this.url + `/reviews/` + review.id, review, options).pipe(
      tap(category => log(`added review`)),
      catchError(this.errorHandlerService.handleError('addReview', null))
    );
  }

  /**
   * Get Books Count
   * @param searchValue
   * @retun  Observable<any>
   */
    getBooksCount(searchValue: string, category: string): Observable<number> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    let bookUrl = this.url + `/books?q=${searchValue}`;
    if (category && category !== '') {
      bookUrl = bookUrl + `&category=${category}`;
    }
    return this.http.get(bookUrl, options).pipe(
      map((books: any[]) => books.length),
      tap(books => log(`fetched book count`)),
      catchError(this.errorHandlerService.handleError('getBooksCount', null))
    );
  }


  /**
   * Get Books
   * @param pageIndex
   * @param pageSize
   * @param searchValue
   * @param category
   * @param hasSort
   * @param sortBy
   * @param sortOrder
   * @retun  Observable<any>
   */
    getBooks(
    pageIndex: number,
    pageSize: number,
    searchValue: string,
    category: string,
    hasSort: boolean,
    sortBy: string,
    sortOrder: string
  ): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    let bookUrl = this.url + `/books?q=${searchValue}&_page=${pageIndex}&_limit=${pageSize}`;

    if (hasSort) {
      bookUrl = bookUrl + `&_sort=${sortBy}&_order=${sortOrder}`;
    }

    if (category && category !== '') {
      bookUrl = bookUrl + `&category=${category}`;
    }
    return this.http.get(bookUrl, options).pipe(
      tap(books => log(`fetched book`)),
      catchError(this.errorHandlerService.handleError('getBook', null))
    );
  }

  /**
   * Get Issued Books Count
   * @param userId, searchValue, status, issuedDate
   * @retun  Observable<any>
   */
    getIssuedBooksCount(userId: string, searchValue: string, status: string, issuedDate: string): Observable<number> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    let bookUrl = this.url + `/issuedbooks?q=${searchValue}`;
    if (userId && userId !== '') {
      bookUrl = bookUrl + `&userId_like=${userId}`;
    }
    if (status && status !== '') {
      bookUrl = bookUrl + `&status=${status}`;
    }
    if (issuedDate && issuedDate !== '') {
      bookUrl = bookUrl + `&issuedDate=${issuedDate}`;
    }
    return this.http.get(bookUrl, options).pipe(
      map((books: any[]) => books.length),
      tap(books => log(`fetched book count`)),
      catchError(this.errorHandlerService.handleError('getBooksCount', null))
    );
  }

  /**
   * Get Issued Books Count By User
   * @param userId
   * @retun  Observable<any>
   */
    getIssuedBooksCountByUser(userId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };

    const url = this.url + `/issuedbooks?userId_like=${userId}&status=Issued&status=Returned`;

    return this.http.get(url, options).pipe(
      tap(books => log(`fecth Issued Book Count By User`)),
      catchError(this.errorHandlerService.handleError('getIssuedBooksCountByUser', null))
    );
  }

  /**
   * Get Issued Books
   * @param pageIndex, pageSize, userId, searchValue, status, issuedDate, hasSort, sortBy, sortOrder
   * @retun  Observable<any>
   */
    getIssuedBooks(
    pageIndex: number,
    pageSize: number,
    userId: string,
    searchValue: string,
    status: string,
    issuedDate: string,
    hasSort: boolean,
    sortBy: string,
    sortOrder: string
  ): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };

    let bookUrl = this.url + `/issuedbooks?q=${searchValue}&_page=${pageIndex}&_limit=${pageSize}`;
    if (userId && userId !== '') {
      bookUrl = bookUrl + `&userId_like=${userId}`;
    }
    if (status && status !== '') {
      bookUrl = bookUrl + `&status=${status}`;
    }
    if (issuedDate && issuedDate !== '') {
      bookUrl = bookUrl + `&issuedDate=${issuedDate}`;
    }
    if (hasSort) {
      bookUrl = bookUrl + `&_sort=${sortBy}&_order=${sortOrder}`;
    }

    return this.http.get(bookUrl, options).pipe(
      tap(books => log(`fetched book`)),
      catchError(this.errorHandlerService.handleError('getBook', null))
    );
  }


  /**
   * Issue Book
   * @param book
   * @retun  Observable<any>
   */
  issueBook(book: IssuedBook): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.post(this.url + '/issuedbooks', book, options).pipe(
      tap(books => log(`issue  books`)),
      catchError(this.errorHandlerService.handleError('issueBook', null))
    );
  }

  /**
   * Get Book 
   * @param bookId
   * @retun  Observable<any>
   */
    getBook(bookId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.url + '/books/' + bookId, options).pipe(
      tap(books => log(`fetched book`)),
      catchError(this.errorHandlerService.handleError('getBook', null))
    );
  }

  /**
   * Update Book
   * @param id
   * @param data
   * @retun  Observable<any>
   */
    updateBook(id: string, data: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.patch(this.url + '/books/' + id, data, options).pipe(
      tap(books => log(`Update book`)),
      catchError(this.errorHandlerService.handleError('updateBook', null))
    );
  }

  /**
   * Update Issued Book
   * @param book
   * @retun  Observable<any>
   */
  updateIssuedBook(book: IssuedBook): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.patch(this.url + '/issuedbooks/' + book.id, book, options).pipe(
      tap(books => log(`issue  books update`)),
      catchError(this.errorHandlerService.handleError('updateIssuedBook', null))
    );
  }
}
