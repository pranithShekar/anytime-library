import { NotificationService } from 'src/app/shared/notification.service';
import { ErrorHandlerService } from './errorHandler.service';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';


/**
 * Error Handler Service Testing
 */
describe('Shared -> ErrorHandlerService', () => {
  let mockNotificationService;
  let service: ErrorHandlerService;
  beforeEach(() => {
    mockNotificationService = jasmine.createSpyObj(['show']);
    TestBed.configureTestingModule({
      providers: [
        ErrorHandlerService,
        {
          provide: NotificationService,
          useValue: mockNotificationService
        }
      ]
    });
    service = TestBed.get(ErrorHandlerService);
  });

  it('should send get request', () => {
    expect(service).toBeTruthy();
  });

  it('should call mockNotificationService on handleError', fakeAsync(() => {
    service
      .handleError()(true)
      .subscribe();

    tick(250);

    expect(mockNotificationService.show).toHaveBeenCalled();
  }));
});
