import { environment } from 'src/environments/environment.prod';
import { TestBed } from '@angular/core/testing';

import { SharedService } from './shared.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ErrorHandlerService } from '../shared/errorHandler.service';
import { Book, IssuedBook } from '../shared/model/book.model';
import { Review } from './model/review.model';

/**
 * Shared Service Service
 */
describe('Shared -> SharedService', () => {
  let mockErrorHandlerService;
  let service: SharedService;
  let httpTestingController: HttpTestingController;
  const book: Book = {
    id: '1111',
    isbn: '1111',
    title: 'title',
    author: 'author',
    description: 'description',
    rating: 1,
    count: 3,
    availableCount: 3,
    location: 'location',
    imageUrl: 'imageUrl',
    category: 'category'
  };
  const review: Review = {
    id: '122',
    bookId: '122',
    title: 'abc',
    date: '12-Aug-2018',
    rating: 3,
    review: 'Good',
    issuedId: '1233',
    userName: 'Abc'
  };

  const issuedBook: IssuedBook = {
    id: '1111',
    isbn: '1111',
    title: 'title',
    author: 'author',
    location: 'location',
    imageUrl: 'imageUrl',
    userId: '123',
    userName: 'Anc',
    issuedDate: '12-Aug-2019',
    returnDate: '22-Aug-2019',
    status: 'issued'
  };

  const data = {
    userId: '123',
    status: 'issued',
    date: '12-Aug-2019',
    pageIndex: 1,
    pageSize: 4,
    searchValue: 'searchValue',
    category: 'category',
    hasSort: true,
    sortBy: 'name',
    sortOrder: 'asc'
  };
  beforeEach(() => {
    mockErrorHandlerService = jasmine.createSpyObj(['handleError']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        SharedService,
        {
          provide: ErrorHandlerService,
          useValue: mockErrorHandlerService
        }
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(SharedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(' should call getConfig with correct url and data', () => {
    service.getConfig().subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/config`);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getCategories with correct url and data', () => {
    service.getCategories().subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/categories`);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getReviews with correct url and data', () => {
    service.getReviews(book.id).subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL + `/reviews?bookId=${book.id}&_sort=date&_order=desc`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call addReview with correct url and data', () => {
    service.addReview(review).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/reviews`);
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual(review);

    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getReview with correct url and data', () => {
    service.getReview(review.issuedId).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/reviews?issuedId=${review.issuedId}`);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call updateReview with correct url and data', () => {
    service.updateReview(review).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/reviews/` + review.id);
    expect(request.request.method).toEqual('PATCH');
    expect(request.request.body).toEqual(review);

    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getBooksCount with correct url and data', () => {
    service.getBooksCount(data.searchValue, data.category).subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/books?q=${data.searchValue}&category=${data.category}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getBooks with correct url and data', () => {
    service
      .getBooks(
        data.pageIndex,
        data.pageSize,
        data.searchValue,
        data.category,
        data.hasSort,
        data.sortBy,
        data.sortOrder
      )
      .subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/books?q=${data.searchValue}&_page=${data.pageIndex}&_limit=${data.pageSize}&_sort=${data.sortBy}&_order=${data.sortOrder}&category=${data.category}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getBooks with (minimal) correct url and data', () => {
    service
      .getBooks(data.pageIndex, data.pageSize, data.searchValue, null, null, data.sortBy, data.sortOrder)
      .subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/books?q=${data.searchValue}&_page=${data.pageIndex}&_limit=${data.pageSize}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getIssuedBooksCount with correct url and data', () => {
    service.getIssuedBooksCount(data.userId, data.searchValue, data.status, data.date).subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/issuedbooks?q=${data.searchValue}&userId_like=${data.userId}&status=${data.status}&issuedDate=${data.date}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getIssuedBooksCount with (minimal) correct url and data', () => {
    service.getIssuedBooksCount(null, data.searchValue, null, null).subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/issuedbooks?q=${data.searchValue}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getIssuedBooksCountByUser with correct url and data', () => {
    service.getIssuedBooksCountByUser(data.userId).subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/issuedbooks?userId_like=${data.userId}&status=Issued&status=Returned`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getIssuedBooks with correct url and data', () => {
    service
      .getIssuedBooks(
        data.pageIndex,
        data.pageSize,
        data.userId,
        data.searchValue,
        data.status,
        data.date,
        data.hasSort,
        data.sortBy,
        data.sortOrder
      )
      .subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/issuedbooks?q=${data.searchValue}&_page=${data.pageIndex}&_limit=${data.pageSize}&userId_like=${data.userId}&status=${data.status}&issuedDate=${data.date}&_sort=${data.sortBy}&_order=${data.sortOrder}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getIssuedBooks with (minimal) correct url and data', () => {
    service
      .getIssuedBooks(
        data.pageIndex,
        data.pageSize,
        null,
        data.searchValue,
        null,
        null,
        null,
        data.sortBy,
        data.sortOrder
      )
      .subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL +
        // tslint:disable-next-line: max-line-length
        `/issuedbooks?q=${data.searchValue}&_page=${data.pageIndex}&_limit=${data.pageSize}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call issueBook with correct url and data', () => {
    service.issueBook(issuedBook).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/issuedbooks`);
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual(issuedBook);
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getBook with correct url and data', () => {
    service.getBook(book.id).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + '/books/' + book.id);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call updateBook with correct url and data', () => {
    service.updateBook(book.id, book).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/books/` + book.id);
    expect(request.request.method).toEqual('PATCH');
    expect(request.request.body).toEqual(book);
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call updateIssuedBook with correct url and data', () => {
    service.updateIssuedBook(issuedBook).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/issuedbooks/` + issuedBook.id);
    expect(request.request.method).toEqual('PATCH');
    expect(request.request.body).toEqual(issuedBook);
    request.flush('success');
    httpTestingController.verify();
  });
});
