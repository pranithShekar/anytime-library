import { ActivatedRoute } from '@angular/router';
import { Menu } from './menu.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export class MenuReference {
  menu: Menu[];
  activatedRoute: ActivatedRoute;
  defaultPath: string;
}


/**
 * Menu Service to handle the menus based on the componets loaded
 */
@Injectable({
  providedIn: 'root'
})
export class MenuService {
  menu$ = new BehaviorSubject<MenuReference>(null);
  selectedMenu$ = new BehaviorSubject<string>(null);

  constructor() {}

  changeMenu(newMenu: Menu[], newActivatedRoute: ActivatedRoute, newDefaultPath: string) {
    this.menu$.next({
      menu: newMenu,
      activatedRoute: newActivatedRoute,
      defaultPath: newDefaultPath
    });
  }

  changeSelectedMenu(newSelectedMenu: string) {
    this.selectedMenu$.next(newSelectedMenu);
  }

  removeMenu() {
    this.menu$.next(null);
    this.selectedMenu$.next(null);
  }
}
