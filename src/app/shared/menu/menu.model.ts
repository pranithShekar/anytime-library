import { Route } from '@angular/router';

/**
 * Menu model
 */

export interface Menu extends Route {
  title: string;
}
