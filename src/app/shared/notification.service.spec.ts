import { NotificationService } from 'src/app/shared/notification.service';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material';

/**
 * Notification Service Testing
 */
describe('Shared -> NotificationService', () => {
  let mockMatSnackBar;
  let service: NotificationService;
  beforeEach(() => {
    mockMatSnackBar = jasmine.createSpyObj(['open']);
    TestBed.configureTestingModule({
      providers: [
        NotificationService,
        {
          provide: MatSnackBar,
          useValue: mockMatSnackBar
        }
      ]
    });
    service = TestBed.get(NotificationService);
  });

  it('should send get request', () => {
    expect(service).toBeTruthy();
  });

  it('should call MatSnackBar  service on show', () => {
    service.show('Simple message');
    expect(mockMatSnackBar.open).toHaveBeenCalled();
  });
});
