import { Component, AfterViewInit } from '@angular/core';
import { AuthenticationService } from './core/authentication.service';
import { UpdateService } from './core/update.service';

/**
 * App Component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private authenticationService: AuthenticationService, private updateService: UpdateService) {
    this.authenticationService.loadAuth2();
  }
}
