import { Injectable } from '@angular/core';

import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanLoad,
  Route,
  UrlSegment,
  CanActivateChild
} from '@angular/router';

/**
 * Admin Route Guard
 */
@Injectable()
export class AdminRouteGuard implements CanActivate, CanLoad, CanActivateChild {
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (sessionStorage.getItem('adminloggedIn') === 'true') {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
  constructor(private router: Router) {}

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (sessionStorage.getItem('adminloggedIn') === 'true') {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (sessionStorage.getItem('adminloggedIn') === 'true') {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}
