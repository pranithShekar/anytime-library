import { TestBed, async } from '@angular/core/testing';
import { AdminRouteGuard } from './admin.route.guard';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * User Route Guard Testing
 */
describe('Core -> AdminRouteGuard', () => {
  let adminRouteGuard: AdminRouteGuard;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      providers: [AdminRouteGuard]
    }).compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    adminRouteGuard = TestBed.get(AdminRouteGuard);
  });

  it('should be able access admin url if logged in', () => {
    sessionStorage.setItem('adminloggedIn', 'true');
    expect(adminRouteGuard.canActivate(null, null)).toBe(true);
    expect(adminRouteGuard.canActivateChild(null, null)).toBe(true);
    expect(adminRouteGuard.canLoad(null, null)).toBe(true);
  });

  it('should not be able access admin url if not logged in', () => {
    sessionStorage.setItem('adminloggedIn', 'false');
    expect(adminRouteGuard.canActivate(null, null)).toBe(false);
    expect(adminRouteGuard.canActivateChild(null, null)).toBe(false);
    expect(adminRouteGuard.canLoad(null, null)).toBe(false);
  });
});
