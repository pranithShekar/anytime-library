import { AuthenticationService } from '../authentication.service';
import { Injectable } from '@angular/core';

import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanActivateChild,
  CanLoad,
  UrlSegment,
  Route
} from '@angular/router';
import { Observable } from 'rxjs';

declare var gapi: any;

/**
 * User Route Guard
 */
@Injectable()
export class UserRouteGuard implements CanActivate, CanActivateChild, CanLoad {
  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
    console.log('canLoad');
    return this.authenticationService.isLoggedIn$;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    console.log('canActivate');
    return this.authenticationService.isLoggedIn$;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    console.log('canActivateChild');
    return this.authenticationService.isLoggedIn$;
  }
}
