import { AuthenticationService } from './../authentication.service';
import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { UserRouteGuard } from './user.route.guard';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * User Route Guard Testing
 */
describe('Core -> UserRouteGuard', () => {
  let userRouteGuard: UserRouteGuard;
  let authenticationService: AuthenticationService;

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      providers: [UserRouteGuard]
    }).compileComponents(); // compile template and css
    authenticationService = TestBed.get(AuthenticationService);
  }));

  // synchronous beforeEach
  beforeEach(() => {
    userRouteGuard = TestBed.get(UserRouteGuard);
  });

  it('should be able access user url if logged in', fakeAsync(() => {
    authenticationService.isLoggedIn$.next(true);
    tick(250);
    userRouteGuard.canActivate(null, null).subscribe(isLoggedin => {
      expect(isLoggedin).toBe(true);
    });
    userRouteGuard.canActivateChild(null, null).subscribe(isLoggedin => {
      expect(isLoggedin).toBe(true);
    });
    userRouteGuard.canLoad(null, null).subscribe(isLoggedin => {
      expect(isLoggedin).toBe(true);
    });
  }));

  it('should not be able access user url if not logged in', fakeAsync(() => {
    authenticationService.isLoggedIn$.next(false);
    tick(250);
    userRouteGuard.canActivate(null, null).subscribe(isLoggedin => {
      expect(isLoggedin).toBe(false);
    });
    userRouteGuard.canActivateChild(null, null).subscribe(isLoggedin => {
      expect(isLoggedin).toBe(false);
    });
    userRouteGuard.canLoad(null, null).subscribe(isLoggedin => {
      expect(isLoggedin).toBe(false);
    });
  }));
});
