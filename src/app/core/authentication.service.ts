import { log } from 'util';
import { Router, ActivatedRoute } from '@angular/router';
import { UserRoleEnum } from './../shared/atl.enum';
import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../shared/model/user.model';
declare var gapi: any;

/**
 * Authentication service
 * Goole Oauth
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public auth2: any;
  public user$: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public isLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public isLoaded$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private zone: NgZone, private router: Router) {}

  /**
   * User Signin by the google
   */
  signIn() {
    this.auth2.signIn().then(user => {
      this.zone.run(() => {
        this.user$.next(this.createUser(user.getBasicProfile()));
        this.isLoggedIn$.next(true);
        this.router.navigate(['user']);
      });
    });
  }

  /**
   * User sign out by google and clears the sessions
   */
  signOut() {
    this.auth2.signOut().then(() => {
      this.zone.run(() => {
        this.user$.next(null);
        this.isLoggedIn$.next(false);
        this.router.navigate(['login']);
        sessionStorage.clear();
      });
    });
  }

  /**
   * Loading the Authentication client
   */
  loadAuth2(): void {
    gapi.load('auth2', () => {
      gapi.auth2
        .init({
          client_id: '116212910038-cvhfid0fuvs0iucdia5ih3noe18sk3nr.apps.googleusercontent.com'
        })
        .then(auth2 => {
          this.zone.run(() => {
            this.auth2 = gapi.auth2.getAuthInstance();
            if (
              sessionStorage.getItem('adminloggedIn') == null ||
              sessionStorage.getItem('adminloggedIn') === 'false'
            ) {
              if (this.auth2.isSignedIn.get()) {
                this.user$.next(
                  this.createUser(
                    gapi.auth2
                      .getAuthInstance()
                      .currentUser.get()
                      .getBasicProfile()
                  )
                );
                this.isLoggedIn$.next(true);
                this.router.navigate(['user']);
                console.log('suceess');
              } else {
                this.isLoggedIn$.next(false);
                this.router.navigate(['login']);
              }
            } else {
              this.adminLogin(sessionStorage.getItem('adminId'));
            }
          });
        });
    });
  }

  /**
   * Admin Login
   */
  adminLogin(adminName) {
    const admin: User = {
      email: adminName,
      name: (adminName as string).toUpperCase(),
      imageUrl: '/assets/images/user-icon.png',
      role: UserRoleEnum.ADMIN,
      userId: adminName
    };
    this.user$.next(admin);
    this.isLoggedIn$.next(true);
    this.router.navigate(['admin']);
    sessionStorage.setItem('adminloggedIn', 'true');
    sessionStorage.setItem('adminId', adminName);
  }

  /**
   * Extracts the user profile info
   * @param userProfile
   */
  createUser(userProfile: any): User {
    return {
      email: userProfile.getEmail(),
      name: (userProfile.getName() as string).toUpperCase(),
      imageUrl: userProfile.getImageUrl(),
      role: UserRoleEnum.USER,
      userId: userProfile.getId()
    };
  }
}
