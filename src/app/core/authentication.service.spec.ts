import { CoreModule } from './core.module';
import { LoginComponent } from './login/login.component';
import { UserRoleEnum } from './../shared/atl.enum';
import { TestBed, tick, fakeAsync } from '@angular/core/testing';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

/**
 * Authentication Service testing
 */
describe('Core -> AuthenticationService', () => {
  let router;
  let service: AuthenticationService;
  let mocKAuth;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, RouterTestingModule.withRoutes([{ path: 'login', component: LoginComponent }])],
      providers: [AuthenticationService]
    });

    router = TestBed.get(Router);
    service = TestBed.get(AuthenticationService);
    mocKAuth = jasmine.createSpyObj(['signIn']);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return User Profile Information when data is passed to createUser', () => {
    const data = {
      getEmail: () => 'abc@gmail.com',
      getName: () => 'abc',
      getImageUrl: () => '/abc.jpeg',
      getId: () => '123'
    };

    const expected = {
      email: 'abc@gmail.com',
      name: 'ABC',
      imageUrl: '/abc.jpeg',
      role: UserRoleEnum.USER,
      userId: '123'
    };

    const actual = service.createUser(data);

    expect(actual).toEqual(expected);
  });

  it('should able to login as admin and redirect to admin page', () => {
    const navigateSpy = spyOn(router, 'navigate');

    service.adminLogin('admin');

    expect(navigateSpy).toHaveBeenCalledWith(['admin']);
  });

  it('should able to login as user and redirect to user page', fakeAsync(() => {
    const useData = {
      getBasicProfile: () => ({
        getEmail: () => 'abc@gmail.com',
        getName: () => 'abc',
        getImageUrl: () => '/abc.jpeg',
        getId: () => '123'
      })
    };
    service.auth2 = {
      signIn: () => new Promise(r1 => r1(useData))
    };
    const navigateSpy = spyOn(router, 'navigate');

    service.signIn();

    tick(250);
    expect(navigateSpy).toHaveBeenCalledWith(['user']);
  }));

  it('should able to logout as user and redirect to login page', fakeAsync(() => {
    service.auth2 = {
      signOut: () => new Promise(r1 => r1())
    };
    const navigateSpy = spyOn(router, 'navigate');
    jasmine.createSpy('gapi');

    service.signOut();

    tick(250);

    expect(navigateSpy).toHaveBeenCalledWith(['login']);
  }));

  it('should load the auth config on call of loadAuth2()', fakeAsync(() => {
    sessionStorage.clear();
    const useData = {
      getBasicProfile: () => ({
        getEmail: () => 'abc@gmail.com',
        getName: () => 'abc',
        getImageUrl: () => '/abc.jpeg',
        getId: () => '123'
      })
    };

    service.auth2 = {
      signOut: () => new Promise(r1 => r1())
    };
    const auth2 = {
      init: () =>
        new Promise(r1 => {
          r1();
        }),
      getAuthInstance: () => ({
        isSignedIn: {
          get: () => false
        },
        currentUser: {
          get: () => useData
        }
      })
    };

    // tslint:disable-next-line: no-string-literal
    window['gapi'] = {
      load: (type, callback) =>
        new Promise(r1 => {
          r1(callback());
        }),

      auth2
    };

    service.loadAuth2();

    tick(250);

    expect(service.auth2).not.toBeNull();
  }));

  it('should redirect to the user page if he has already logged in', fakeAsync(() => {
    const navigateSpy = spyOn(router, 'navigate');

    sessionStorage.clear();
    const useData = {
      getBasicProfile: () => ({
        getEmail: () => 'abc@gmail.com',
        getName: () => 'abc',
        getImageUrl: () => '/abc.jpeg',
        getId: () => '123'
      })
    };

    service.auth2 = {
      signOut: () => new Promise(r1 => r1())
    };
    const auth2 = {
      init: () =>
        new Promise(r1 => {
          r1();
        }),
      getAuthInstance: () => ({
        isSignedIn: {
          get: () => true
        },
        currentUser: {
          get: () => useData
        }
      })
    };

    // tslint:disable-next-line: no-string-literal
    window['gapi'] = {
      load: (type, callback) =>
        new Promise(r1 => {
          r1(callback());
        }),

      auth2
    };

    service.loadAuth2();

    tick(250);

    expect(navigateSpy).toHaveBeenCalledWith(['user']);
  }));

  it('should call the adminLogin if the admin has logged in', fakeAsync(() => {
    sessionStorage.setItem('adminloggedIn', 'true');
    sessionStorage.setItem('adminId', 'Admin');
    const spyOnFunc = spyOn(AuthenticationService.prototype, 'adminLogin');
    const useData = {
      getBasicProfile: () => ({
        getEmail: () => 'abc@gmail.com',
        getName: () => 'abc',
        getImageUrl: () => '/abc.jpeg',
        getId: () => '123'
      })
    };

    service.auth2 = {
      signOut: () => new Promise(r1 => r1())
    };
    const auth2 = {
      init: () =>
        new Promise(r1 => {
          r1();
        }),
      getAuthInstance: () => ({
        isSignedIn: {
          get: () => true
        },
        currentUser: {
          get: () => useData
        }
      })
    };

    // tslint:disable-next-line: no-string-literal
    window['gapi'] = {
      load: (type, callback) =>
        new Promise(r1 => {
          r1(callback());
        }),

      auth2
    };

    service.loadAuth2();

    tick(250);

    expect(spyOnFunc).toHaveBeenCalled();
  }));
});
