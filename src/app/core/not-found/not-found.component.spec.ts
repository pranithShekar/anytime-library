import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFoundComponent } from './not-found.component';

/**
 * Not found page testing
 */
describe('Core -> NotFoundComponent', () => {
  let component: NotFoundComponent;
  let fixture: ComponentFixture<NotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NotFoundComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create NotFoundComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should show contain Page not found message', () => {
    const expected = 'Page not found';
    const actual = fixture.nativeElement.querySelector('div').textContent;
    expect(actual).toContain(expected);
  });
});
