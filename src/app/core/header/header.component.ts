import { ActivatedRoute, Router } from '@angular/router';
import { Menu } from './../../shared/menu/menu.model';
import { MenuReference } from './../../shared/menu/menu.service';
import { MenuService } from '../../shared/menu/menu.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../authentication.service';

/**
 * Header component
 * Shows the menu items
 * shows the user menu
 * logout option
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  menu: Menu[];
  menuActivatedRoute: ActivatedRoute;
  defaultPath: string;
  selectedMenu: string;
  destroy$ = new Subject();

  constructor(
    public authenticationService: AuthenticationService,
    private menuService: MenuService,
    private router: Router
  ) {}

  ngOnInit() {
    this.menuService.menu$.pipe(takeUntil(this.destroy$)).subscribe((menuRef: MenuReference) => {
      if (menuRef != null) {
        this.menu = menuRef.menu;
        this.menuActivatedRoute = menuRef.activatedRoute;
        this.defaultPath = menuRef.defaultPath;
      } else {
        this.menu = [];
        this.menuActivatedRoute = null;
        this.defaultPath = '';
      }
    });

    this.menuService.selectedMenu$.pipe(takeUntil(this.destroy$)).subscribe((selectedMenu: string) => {
      if (selectedMenu != null) {
        this.selectedMenu = selectedMenu;
      } else {
        this.selectedMenu = null;
      }
    });
  }

  menuRoute(path: string) {
    this.router.navigate(['.', path], { relativeTo: this.menuActivatedRoute });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
