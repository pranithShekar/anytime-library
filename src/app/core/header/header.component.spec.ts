import { AuthenticationService } from 'src/app/core/authentication.service';
import { By } from '@angular/platform-browser';
import { Menu } from './../../shared/menu/menu.model';
import { UserRoleEnum } from './../../shared/atl.enum';
import { MatMenuModule } from '@angular/material/menu';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { MenuService } from 'src/app/shared/menu/menu.service';
import { UserMenu } from 'src/app/user/user.menu';
import { Router } from '@angular/router';

describe('Core -> HeaderComponent', () => {
  let router;
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let menuService;
  let authenticationService: AuthenticationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatMenuModule, RouterTestingModule.withRoutes([])],
      declarations: [HeaderComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    menuService = TestBed.get(MenuService);
    authenticationService = TestBed.get(AuthenticationService);
    fixture.detectChanges();
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(HeaderComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load menus properly form menu service', fakeAsync(() => {
    const menu: Menu[] = UserMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    const expected = menu;

    const actual = component.menu;

    expect(actual).toEqual(expected);
  }));

  it('should load selected menus properly form the menu service', fakeAsync(() => {
    const selectedmenu = 'hi';
    menuService.changeSelectedMenu(selectedmenu);

    tick(250);

    const expected = selectedmenu;
    const actual = component.selectedMenu;
    expect(actual).toEqual(expected);
  }));

  it('should route to selected menu path', () => {
    const navigateSpy = spyOn(router, 'navigate');

    component.menuRoute('admin');

    expect(navigateSpy).toHaveBeenCalledWith(['.', 'admin'], { relativeTo: null });
  });

  it('should title on the Header', () => {
    const expected = 'Library';
    const actual = fixture.debugElement.query(By.css('.title')).nativeElement.textContent;
    expect(actual).toContain(expected);
  });

  it('should render correct number of the menu', fakeAsync(() => {
    const menu: Menu[] = UserMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    fixture.detectChanges();
    const expected = UserMenu.length;
    const actual = fixture.debugElement.queryAll(By.css('.menu__item')).length;
    expect(actual).toEqual(expected * 2);
  }));

  it('should render correct highlighed of the menu', fakeAsync(() => {
    const menu: Menu[] = UserMenu;
    const selectedmenu = UserMenu[0].path;
    const selectedmenuTitle = UserMenu[0].title;
    menuService.changeSelectedMenu(selectedmenu);
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    fixture.detectChanges();
    const expected = selectedmenuTitle;
    const actual = fixture.debugElement.query(By.css('.menu__item--selected')).nativeElement.textContent;
    expect(actual).toContain(expected);
  }));

  it('should render correct user name on the left if logged in ', fakeAsync(() => {
    const user = {
      email: 'abc@gmail.com',
      name: 'ABC',
      imageUrl: '/abc.jpeg',
      role: UserRoleEnum.USER,
      userId: '123'
    };

    authenticationService.isLoggedIn$.next(true);
    authenticationService.user$.next(user);

    tick(250);

    fixture.detectChanges();
    const expected = user.name;
    const actual = fixture.debugElement.query(By.css('.user__name')).nativeElement.textContent;
    expect(actual).toContain(expected);
  }));

  // it(`should call signout when signout is clicked`, fakeAsync(() => {
  //   const user = {
  //     email: 'abc@gmail.com',
  //     name: 'ABC',
  //     imageUrl: '/abc.jpeg',
  //     role: UserRoleEnum.USER,
  //     userId: '123'
  //   };
  //   spyOn(AuthenticationService.prototype, 'signOut').and.callFake(() => {});
  //   authenticationService.isLoggedIn$.next(true);
  //   authenticationService.user$.next(user);

  //   tick(250);

  //   fixture.detectChanges();

  //   fixture.debugElement.query(By.css('.user__name')).nativeElement.click();

  //   fixture.detectChanges();

  //   fixture.debugElement.query(By.css('.logout__button')).nativeElement.click();

  //   expect(AuthenticationService.prototype.signOut).toHaveBeenCalled();
  // }));

  it(`should call signout when menuRoute when menu-item is clicked`, fakeAsync(() => {
    spyOn(component, 'menuRoute').and.callFake(() => {});

    const menu: Menu[] = UserMenu;
    const selectedmenu = UserMenu[0].path;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    fixture.detectChanges();

    fixture.debugElement.query(By.css('.menu__item')).triggerEventHandler('click', { stopPropagation: () => {} });

    expect(component.menuRoute).toHaveBeenCalledWith(selectedmenu);
  }));
});
