import { Injectable } from '@angular/core';

import { MatSnackBar } from '@angular/material';
import { SwUpdate } from '@angular/service-worker';

/**
 * UpdateService
 * Will Prompt user to reload when there is a new update
 */
@Injectable()
export class UpdateService {
  constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar) {
    this.swUpdate.available.subscribe(evt => {
      const snack = this.snackbar.open('Update Available', 'Reload', {
        duration: 10000,
        panelClass: 'notification__background'
      });

      snack.onAction().subscribe(() => {
        this.windowReload();
      });
    });
  }

  windowReload() {
    window.location.reload();
  }
}
