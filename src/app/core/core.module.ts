import { HttpClientModule } from '@angular/common/http';
import { UpdateService } from './update.service';
import { LoginService } from './login/login.service';
import { RouterModule } from '@angular/router';
import { CoreMaterialModule } from './core.material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HeaderComponent } from './header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { UserRouteGuard } from './route-guards/user.route.guard';
import { AdminRouteGuard } from './route-guards/admin.route.guard';

/**
 * Core module
 */
@NgModule({
  declarations: [LoginComponent, NotFoundComponent, HeaderComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    CoreMaterialModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    SharedModule
  ],
  exports: [LoginComponent, NotFoundComponent, HeaderComponent],
  providers: [LoginService, UpdateService, UserRouteGuard, AdminRouteGuard]
})
export class CoreModule {}
