import { ErrorHandlerService } from 'src/app/shared/errorHandler.service';
import { TestBed } from '@angular/core/testing';
import { LoginService } from './login.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

/**
 * Login Service Testing
 */
describe('Core -> LoginService', () => {
  let mockErrorHandlerService;
  let service: LoginService;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    mockErrorHandlerService = jasmine.createSpyObj(['handleError']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        LoginService,
        {
          provide: ErrorHandlerService,
          useValue: mockErrorHandlerService
        }
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(LoginService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(' should call login get with correct url', () => {
    const userName = 'admin';
    const password = 'password';
    service.login(userName, password).subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL + `/admin?userName=${userName}&password=${password}`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });
});
