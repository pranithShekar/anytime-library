import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { log } from 'util';
import { environment } from 'src/environments/environment.prod';
import { ErrorHandlerService } from 'src/app/shared/errorHandler.service';

/**
 * Login Service
 */
@Injectable()
export class LoginService {
  private url = environment.API_URL;

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) {}

  /**
   * Login
   * @param userName
   * @param password
   * @return Observable<any>
   */
  login(userName: string, password: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };
    return this.http.get(this.url + `/admin?userName=${userName}&password=${password}`, options).pipe(
      tap(() => log(`login cofig`)),
      catchError(this.errorHandlerService.handleError('getConfig', null))
    );
  }
}
