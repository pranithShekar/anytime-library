import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginService } from './login.service';
import { NotificationService } from 'src/app/shared/notification.service';
import { AuthenticationService } from '../authentication.service';
import { of } from 'rxjs';

/**
 * Admin login Testing
 */
describe('Core -> LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let mockLoginService;
  let mockNotificationService;
  let mockAuthenticationService;

  beforeEach(async(() => {
    mockLoginService = jasmine.createSpyObj(['login']);
    mockNotificationService = jasmine.createSpyObj(['show']);
    mockAuthenticationService = jasmine.createSpyObj(['signIn', 'adminLogin']);

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule.withRoutes([])],
      declarations: [LoginComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: LoginService,
          useValue: mockLoginService
        },
        {
          provide: NotificationService,
          useValue: mockNotificationService
        },
        {
          provide: AuthenticationService,
          useValue: mockAuthenticationService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call auth SignIn fucntion from signIn menthod', () => {
    mockAuthenticationService.signIn.and.returnValue(of(true));

    component.signIn();

    expect(mockAuthenticationService.signIn).toHaveBeenCalled();
  });

  it('should call admin login service from login menthod', () => {
    const userName = 'admin';
    const password = 'password';
    mockLoginService.login.and.returnValue(of([true]));
    mockAuthenticationService.adminLogin.and.returnValue(of(true));

    component.loginForm.patchValue({
      userName,
      password
    });

    component.login();

    expect(mockLoginService.login).toHaveBeenCalledWith(userName, password);
  });

  it('should not call admin login service from login menthod if form is invalid', () => {
    mockLoginService.login.and.returnValue(of([true]));
    mockAuthenticationService.adminLogin.and.returnValue(of(true));

    component.login();

    expect(mockLoginService.login).not.toHaveBeenCalled();
  });

  it('should call notification service incase of the invalid login', () => {
    const userName = 'admin';
    const password = 'password';
    mockLoginService.login.and.returnValue(of(null, []));
    mockNotificationService.show.and.returnValue();

    component.loginForm.patchValue({
      userName,
      password
    });

    component.login();

    expect(mockNotificationService.show).toHaveBeenCalled();
  });

  // it('should call notification service incase of the invalid login', () => {
  //   const userName = 'admin';
  //   const password = 'password';
  //   mockLoginService.login.and.returnValue(of());
  //   mockNotificationService.show.and.returnValue();

  //   component.loginForm.patchValue({
  //     userName,
  //     password
  //   });

  //   component.login();

  //   expect(mockNotificationService.show).toHaveBeenCalled();
  // });

  it(`should call signin when use click on signin from google`, fakeAsync(() => {
    spyOn(component, 'signIn').and.callFake(() => {});

    fixture.detectChanges();

    fixture.debugElement.query(By.css('.user__signin')).triggerEventHandler('click', { stopPropagation: () => {} });

    expect(component.signIn).toHaveBeenCalled();
  }));

  it(`should call login when admin click on login`, () => {
    spyOn(component, 'login').and.callFake(() => {});
    mockLoginService.login.and.returnValue(of([]));
    mockNotificationService.show.and.returnValue();

    fixture.detectChanges();

    fixture.debugElement.query(By.css('.admin__signin__link')).nativeElement.click();

    fixture.detectChanges();

    fixture.debugElement.query(By.css('.admin__login')).nativeElement.click();
  });
});
