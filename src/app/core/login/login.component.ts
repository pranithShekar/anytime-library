import { NotificationService } from './../../shared/notification.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MenuService } from 'src/app/shared/menu/menu.service';
import { AuthenticationService } from '../authentication.service';
import { LoginService } from './login.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * Login Component
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  asAdmin = false;
  userName: string;
  password: string;
  loginForm: FormGroup;
  hidePassword = true;
  destroy$ = new Subject();

  constructor(
    private menuService: MenuService,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private service: LoginService,
    private notificationService: NotificationService
  ) {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.menuService.removeMenu();
  }

  /**
   * Calls the authentication service for user sign in
   */
  signIn(): void {
    this.authenticationService.signIn();
  }
  /**
   * Calls the authentication service for admin sign in
   */
  ngOnInit() {}

  login() {
    if (this.loginForm.valid) {
      this.service
        .login(this.loginForm.value.userName, this.loginForm.value.password)
        .pipe(takeUntil(this.destroy$))
        .subscribe((data: any[]) => {
          if (data) {
            if (data.length === 1) {
              this.notificationService.show('Login Successfull');
              this.authenticationService.adminLogin(this.loginForm.value.userName);
            } else {
              this.notificationService.show('Invalid credentials. Try agin.');
            }
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
