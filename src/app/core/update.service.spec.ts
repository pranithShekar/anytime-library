import { MatSnackBar } from '@angular/material';
import { SwUpdate } from '@angular/service-worker';
import { TestBed, inject, tick, fakeAsync } from '@angular/core/testing';
import { UpdateService } from './update.service';
import { of } from 'rxjs';

/**
 * Update Service testing 
 */
describe('Core -> UpdateService', () => {
  let mockSwUpdate;
  let mockWindowReload;
  let mockMatSnackBar;
  let spyOnReload;

  beforeEach(() => {
    spyOnReload = spyOn(UpdateService.prototype, 'windowReload').and.callFake(() => {});

    mockSwUpdate = {
      available: of(true)
    };

    mockWindowReload = {
      reload: () => {}
    };

    mockMatSnackBar = {
      open: () => ({
        onAction: () => of(true)
      })
    };

    TestBed.configureTestingModule({
      providers: [
        UpdateService,
        {
          provide: SwUpdate,
          useValue: mockSwUpdate
        },
        {
          provide: MatSnackBar,
          useValue: mockMatSnackBar
        }
      ]
    });
  });

  it('should send get request', () => {
    const serviceCreated: UpdateService = TestBed.get(UpdateService);
    expect(serviceCreated).toBeTruthy();
  });

  it('should call windows reload on snack-bar action', fakeAsync(() => {
    const service: UpdateService = TestBed.get(UpdateService);

    tick(250);

    expect(service.windowReload).toHaveBeenCalled();
  }));
});
