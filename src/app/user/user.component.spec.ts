import { By } from '@angular/platform-browser';
import { MenuService } from './../shared/menu/menu.service';
import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

/**
 * User Component Testing
 */

describe('User -> UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;
  let mockMenuService;

  beforeEach(async(() => {
    mockMenuService = jasmine.createSpyObj(['changeMenu']);
    TestBed.configureTestingModule({
      declarations: [UserComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: null
        },
        {
          provide: MenuService,
          useValue: mockMenuService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call changeMenu of menu service', () => {
    expect(mockMenuService.changeMenu).toHaveBeenCalled();
  });

  it('should render one router-outlet', () => {
    const expected = 1;

    const actual = fixture.debugElement.queryAll(By.css('router-outlet')).length;

    expect(actual).toBe(expected);
  });
});
