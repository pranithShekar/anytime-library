import { Router, ActivatedRoute } from '@angular/router';
import { UserMenu } from './user.menu';
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../shared/menu/menu.service';

/**
 * User Component
 */
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  menu = UserMenu;

  constructor(private menuService: MenuService, private activatedRoute: ActivatedRoute) {
    this.menuService.changeMenu(this.menu, this.activatedRoute, 'dashboard');
  }

  ngOnInit() {}
}
