import { environment } from 'src/environments/environment.prod';
import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ErrorHandlerService } from '../shared/errorHandler.service';

/**
 * User Service Testing
 */
describe('User -> UserService', () => {
  let mockErrorHandlerService;
  let service: UserService;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    mockErrorHandlerService = jasmine.createSpyObj(['handleError']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserService,
        {
          provide: ErrorHandlerService,
          useValue: mockErrorHandlerService
        }
      ]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(' should call getIssuedBooksCountByUser with correct url', () => {
    const userId = '123';
    service.getIssuedBooksCountByUser(userId).subscribe(() => {});

    const request = httpTestingController.expectOne(
      environment.API_URL + `/issuedbooks?userId_like=${userId}&status=Issued&status=Renewed`
    );
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });

  it(' should call getTotalIssuedBooksCountByUser with correct url', () => {
    const userId = '123';
    service.getTotalIssuedBooksCountByUser(userId).subscribe(() => {});

    const request = httpTestingController.expectOne(environment.API_URL + `/issuedbooks?userId_like=${userId}`);
    expect(request.request.method).toEqual('GET');
    request.flush('success');
    httpTestingController.verify();
  });
});
