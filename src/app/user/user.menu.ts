import { IssuedBookListComponent } from './../shared/issued-book-list/issued-book-list.component';
import { UserRoleEnum } from './../shared/atl.enum';
import { Menu } from './../shared/menu/menu.model';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { BookListComponent } from '../shared/book-list/book-list.component';

/**
 * User Menu Configuration
 */
export const UserMenu: Menu[] = [
  {
    path: 'books',
    component: BookListComponent,
    title: 'All Books',
    data: { role: UserRoleEnum.USER, type: 'books' }
  },
  {
    path: 'mybooks',
    component: IssuedBookListComponent,
    title: 'My Books',
    data: { role: UserRoleEnum.USER, type: 'mybooks' }
  }
];
