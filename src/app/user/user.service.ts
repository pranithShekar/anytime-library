import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { log } from 'util';
import { environment } from 'src/environments/environment.prod';
import { ErrorHandlerService } from '../shared/errorHandler.service';


/**
 * User Service
 */
@Injectable()
export class UserService {
  private url = environment.API_URL;

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) {}

  /** 
   * Get Issued Books Count By User
   * @param userId
   * @resturn Observable<any>
   */
  getIssuedBooksCountByUser(userId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };

    const url = this.url + `/issuedbooks?userId_like=${userId}&status=Issued&status=Renewed`;

    return this.http.get(url, options).pipe(
      tap(() => log(`fecth Issued Book Count By User`)),
      catchError(this.errorHandlerService.handleError('getIssuedBooksCountByUser', null))
    );
  }

  /** 
   * Get Total Issued Books Count By User
   * @param userId
   * @resturn Observable<any>
   */
    getTotalIssuedBooksCountByUser(userId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options = { headers };

    const url = this.url + `/issuedbooks?userId_like=${userId}`;

    return this.http.get(url, options).pipe(
      tap(() => log(`fecth Issued Book Count By User`)),
      catchError(this.errorHandlerService.handleError('getIssuedBooksCountByUser', null))
    );
  }
}
