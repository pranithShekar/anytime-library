import { UserService } from './user.service';
import { UserMaterialModule } from './user.material';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserRoutingModule } from './user.route';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { SharedModule } from '../shared/shared.module';
import { UserProfileComponent } from './user-profile/user-profile.component';

/**
 * User Module
 */
@NgModule({
  declarations: [UserComponent, UserDashboardComponent, UserProfileComponent],
  imports: [CommonModule, UserRoutingModule, UserMaterialModule, SharedModule],
  entryComponents: [UserProfileComponent],
  providers: [UserService]
})
export class UserModule {}
