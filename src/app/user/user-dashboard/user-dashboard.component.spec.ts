import { By } from '@angular/platform-browser';
import { MenuService } from './../../shared/menu/menu.service';
import { UserMenu } from './../user.menu';
import { MatDialog } from '@angular/material/dialog';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { UserDashboardComponent } from './user-dashboard.component';
import { NO_ERRORS_SCHEMA, Directive, Input, HostListener } from '@angular/core';
import { Menu } from 'src/app/shared/menu/menu.model';

/**
 * User Dashboard testing
 */
describe('User -> UserDashboardComponent', () => {
  let component: UserDashboardComponent;
  let fixture: ComponentFixture<UserDashboardComponent>;
  let mockMatDialog;
  let menuService: MenuService;

  @Directive({
    // tslint:disable-next-line: directive-selector
    selector: '[routerLink]'
  })
  class RouterLinkDirectiveStub {
    // tslint:disable-next-line: no-input-rename
    @Input('routerLink') linkParams: any;
    navigatedTo: any = null;

    @HostListener('click')
    onClick() {
      this.navigatedTo = this.linkParams;
    }
  }

  beforeEach(async(() => {
    mockMatDialog = jasmine.createSpyObj(['open']);
    TestBed.configureTestingModule({
      declarations: [UserDashboardComponent, RouterLinkDirectiveStub],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: MatDialog,
          useValue: mockMatDialog
        }
      ]
    }).compileComponents();
    menuService = TestBed.get(MenuService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load menus properly form menu service', fakeAsync(() => {
    const menu: Menu[] = UserMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    const expected = menu;

    const actual = component.menu;

    expect(actual).toEqual(expected);
  }));

  it('should open dailog in  openProfileDialog', () => {
    component.openProfileDialog();
    expect(mockMatDialog.open).toHaveBeenCalled();
  });

  it('should render Dashboard on the Header', () => {
    const expected = 'Dashboard';
    const actual = fixture.debugElement.query(By.css('.mat-display-1')).nativeElement.textContent;
    expect(actual).toContain(expected);
  });

  it('should render correct number of the menu', fakeAsync(() => {
    const menu: Menu[] = UserMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    fixture.detectChanges();
    const expected = UserMenu.length;
    const actual = fixture.debugElement.queryAll(By.css('.menu__card')).length;
    expect(actual).toEqual(expected);
  }));

  it('should call  openProfileDialog on click of profile button', fakeAsync(() => {
    spyOn(component, 'openProfileDialog');

    fixture.debugElement.query(By.css('.profile__button')).nativeElement.click();
    expect(component.openProfileDialog).toHaveBeenCalled();
  }));

  it(`it should have correct route for menus`, fakeAsync(() => {
    const menu: Menu[] = UserMenu;
    menuService.changeMenu(menu, null, '/default');

    tick(250);

    fixture.detectChanges();

    const routerLink = fixture.debugElement
      .query(By.directive(RouterLinkDirectiveStub))
      .injector.get(RouterLinkDirectiveStub);

    fixture.debugElement.query(By.css('.menu__card')).nativeElement.click();

    const expected = ['../', UserMenu[0].path];
    const actual = routerLink.navigatedTo;
    expect(expected).toEqual(actual);
  }));
});
