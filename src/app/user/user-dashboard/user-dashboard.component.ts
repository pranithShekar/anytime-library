import { Menu } from './../../shared/menu/menu.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuService, MenuReference } from 'src/app/shared/menu/menu.service';
import { MatDialog } from '@angular/material/dialog';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

/**
 * User Dashboard component
 * Displays the Dashboard with user menu item
 * Link to open Profile Info
 */
@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit, OnDestroy {
  menu: Menu[];
  destroy$ = new Subject();

  constructor(private menuService: MenuService, private dialog: MatDialog) {}
  ngOnInit() {
    this.menuService.menu$.pipe(takeUntil(this.destroy$)).subscribe((menuRef: MenuReference) => {
      if (menuRef != null) {
        this.menu = menuRef.menu;
      }
    });

    this.menuService.changeSelectedMenu('');
  }

  /**
   * Opens the Profile info popup
   */
  openProfileDialog(): void {
    const dialogRef = this.dialog.open(UserProfileComponent, {
      width: '400px',
      autoFocus: false
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
