import { UserRouteGuard } from '../core/route-guards/user.route.guard';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserMenu } from './user.menu';
import { UserComponent } from './user.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**
 * User Routes Configuration
 */
const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    canActivate: [UserRouteGuard],
    canActivateChild: [UserRouteGuard],
    children: [
      {
        path: 'dashboard',
        component: UserDashboardComponent
      },
      ...UserMenu,
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
