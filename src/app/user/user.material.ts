import { MatSelectModule } from '@angular/material/select';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
/**
 * User Material Imports
 */
@NgModule({
  exports: [
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule
  ]
})
export class UserMaterialModule {}
