import { By } from '@angular/platform-browser';
import { UserRoleEnum } from './../../shared/atl.enum';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { UserService } from './../user.service';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { UserProfileComponent } from './user-profile.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { fsync } from 'fs';
import { of } from 'rxjs';

describe('User -> UserProfileComponent', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;
  let mockUserService;
  let authenticationService: AuthenticationService;

  beforeEach(async(() => {
    mockUserService = jasmine.createSpyObj(['getIssuedBooksCountByUser', 'getTotalIssuedBooksCountByUser']);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      declarations: [UserProfileComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: UserService,
          useValue: mockUserService
        }
      ]
    }).compileComponents();

    authenticationService = TestBed.get(AuthenticationService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call getBookInfo after loading the user', fakeAsync(() => {
    spyOn(component, 'getBookInfo').and.callFake(() => {});
    const user = {
      email: 'abc@gmail.com',
      name: 'ABC',
      imageUrl: '/abc.jpeg',
      role: UserRoleEnum.USER,
      userId: '123'
    };
    authenticationService.user$.next(user);

    tick(250);

    expect(component.getBookInfo).toHaveBeenCalled();
  }));

  it('should call getIssuedBooksCountByUser for getting book issue info of the user', fakeAsync(() => {
    mockUserService.getTotalIssuedBooksCountByUser.and.returnValue(of(true));
    mockUserService.getIssuedBooksCountByUser.and.returnValue(of(true));

    const user = {
      email: 'abc@gmail.com',
      name: 'ABC',
      imageUrl: '/abc.jpeg',
      role: UserRoleEnum.USER,
      userId: '123'
    };
    component.user = user;
    component.getBookInfo();

    expect(mockUserService.getIssuedBooksCountByUser).toHaveBeenCalledWith('123');
  }));

  it('should call getTotalIssuedBooksCountByUser for getting book issue info of the user', fakeAsync(() => {
    mockUserService.getTotalIssuedBooksCountByUser.and.returnValue(of(true, null));
    mockUserService.getIssuedBooksCountByUser.and.returnValue(of(true, null));
    const user = {
      email: 'abc@gmail.com',
      name: 'ABC',
      imageUrl: '/abc.jpeg',
      role: UserRoleEnum.USER,
      userId: '123'
    };
    component.user = user;
    component.getBookInfo();
    expect(mockUserService.getTotalIssuedBooksCountByUser).toHaveBeenCalledWith('123');
  }));

  it(`should call getTotalIssuedBooksCountByUser for getting book issue info of the user and
   set it to respective variable`, fakeAsync(() => {
    mockUserService.getTotalIssuedBooksCountByUser.and.returnValue(of([true, true, true, true]));
    mockUserService.getIssuedBooksCountByUser.and.returnValue(of([true, true]));
    const user = {
      email: 'abc@gmail.com',
      name: 'ABC',
      imageUrl: '/abc.jpeg',
      role: UserRoleEnum.USER,
      userId: '123'
    };
    component.user = user;
    component.getBookInfo();

    fixture.detectChanges();

    expect(component.totalBooksIssued).toEqual(4);
    expect(component.issuedBooks).toEqual(2);
  }));

  describe('should call render proper user info on the popup()', () => {
    const user = {
      email: 'abc@gmail.com',
      name: 'ABC',
      imageUrl: '/abc.jpeg',
      role: UserRoleEnum.USER,
      userId: '123'
    };
    beforeEach(() => {
      mockUserService.getTotalIssuedBooksCountByUser.and.returnValue(of([true, true, true, true]));
      mockUserService.getIssuedBooksCountByUser.and.returnValue(of([true, true]));
      component.user = user;
      fixture.detectChanges();
    });

    it(`userId`, fakeAsync(() => {
      component.getBookInfo();
      expect(fixture.debugElement.query(By.css('.user__id')).nativeElement.textContent).toContain(user.userId);
    }));

    it(`name`, fakeAsync(() => {
      expect(fixture.debugElement.query(By.css('.user__name')).nativeElement.textContent).toContain(user.name);
    }));

    it(`email`, fakeAsync(() => {
      expect(fixture.debugElement.query(By.css('.user__email')).nativeElement.textContent).toContain(user.email);
    }));

    it(`totalBooksIssued`, fakeAsync(() => {
      expect(fixture.debugElement.query(By.css('.user__totalBooksIssued')).nativeElement.textContent).toContain(
        component.totalBooksIssued
      );
    }));

    it(`issuedBooks`, fakeAsync(() => {
      expect(fixture.debugElement.query(By.css('.user__issuedBooks')).nativeElement.textContent).toContain(
        component.issuedBooks
      );
    }));
  });
});
