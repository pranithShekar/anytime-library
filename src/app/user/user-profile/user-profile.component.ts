import { User } from './../../shared/model/user.model';
import { AuthenticationService } from './../../core/authentication.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../user.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

/**
 * User profile
 * Displays the user profile info from auth server
 * shows the books issue status
 */
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  totalBooksIssued = 0;
  issuedBooks = 0;
  destroy$ = new Subject();
  categories: any[] = ['Technology', 'Business', 'Fiction', 'Management'];
  user: User;
  constructor(private service: UserService, private authenticationService: AuthenticationService) {
    this.authenticationService.user$.pipe(takeUntil(this.destroy$)).subscribe((user: User) => {
      if (user) {
        this.user = user;
        this.getBookInfo();
      }
    });
  }

  ngOnInit() {}

  /**
   * Get the user book issue info
   */
  getBookInfo() {
    this.service
      .getIssuedBooksCountByUser(this.user.userId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((books: any[]) => {
        if (books) {
          this.issuedBooks = books.length;
        }
      });

    this.service
      .getTotalIssuedBooksCountByUser(this.user.userId)
      .pipe(takeUntil(this.destroy$))
      .subscribe((books: any[]) => {
        if (books) {
          this.totalBooksIssued = books.length;
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
