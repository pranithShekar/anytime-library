import { AuthenticationService } from 'src/app/core/authentication.service';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { UpdateService } from './core/update.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

/**
 * App Component testing
 */
describe('App -> AppComponent', () => {
  let componentFixture: ComponentFixture<AppComponent>;
  let mockAuthenticationService;

  beforeEach(async(() => {
    mockAuthenticationService = jasmine.createSpyObj(['loadAuth2']);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AppComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: UpdateService,
          useValue: null
        },
        {
          provide: AuthenticationService,
          useValue: mockAuthenticationService
        }
      ]
    }).compileComponents();

    componentFixture = TestBed.createComponent(AppComponent);
  }));

  it('should create the app', () => {
    const app = componentFixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should call loadAuth2 service', () => {
    mockAuthenticationService.loadAuth2.and.returnValue(of(true));

    expect(mockAuthenticationService.loadAuth2).toHaveBeenCalled();
  });

  it('should render one header component', () => {
    const expected = 1;

    const actual = componentFixture.debugElement.queryAll(By.css('app-header')).length;

    expect(actual).toBe(expected);
  });

  it('should render one router-outlet', () => {
    const expected = 1;

    const actual = componentFixture.debugElement.queryAll(By.css('router-outlet')).length;

    expect(actual).toBe(expected);
  });
});
